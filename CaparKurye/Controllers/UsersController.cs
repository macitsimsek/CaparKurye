﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CaparKurye.AC;
using System.Configuration;
using Newtonsoft.Json;

namespace CaparKurye.Controllers
{
    public class UsersController : Controller
    {
        private AC.AppContext db = new AC.AppContext();

        public void sendForgotPasswordMail(Users users)
        {
            string recievermail = ConfigurationManager.AppSettings["recievermail"];
            string mailaddress = ConfigurationManager.AppSettings["mailaddress"];
            string username = ConfigurationManager.AppSettings["username"];
            string password = ConfigurationManager.AppSettings["password"];
            string pop3 = ConfigurationManager.AppSettings["smtp"];


            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage(mailaddress, recievermail);

            message.IsBodyHtml = true;
            message.Subject = "Şifre Bilgileri - Çapar Kurye";
            message.Body = "<p>Çapar Kurye web sitesinden gelen mail aşağıdaki gibidir.</p>"
                + "<b>Email Adresi: </b>" + users.email + "<br><br>"
                + "<b>Şifre: </b>" + users.password + "<br><br>";
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Host = pop3;
            smtp.Port = 587;

            System.Net.Mail.MailAddress rplyadres = new System.Net.Mail.MailAddress(users.email);
            message.ReplyTo = rplyadres;

            smtp.Credentials = new System.Net.NetworkCredential(username, password);
            smtp.Send(message);
        }

        public bool UserCheck()
        {
            if (Session["userID"] != null && Session["userAdSoyad"] != null)
            {
                TempData["Message"] = null;
                return true;
            }
            else
            {
                TempData["Message"] = "Lütfen Kullanıcı Girişi Yapınız";
                return false;
            }
        }

        public bool UserCheckBasic()
        {
            if (Session["userID"] != null && Session["userAdSoyad"] != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Post()
        {
            var response = Request["g-recaptcha-response"];
            const string secret = "6LfTbj8UAAAAAH2yTZ73DVWh3yM3aPmE7eec1Kf5";
            //Kendi Secret keyinizle değiştirin.

            var client = new WebClient();
            var reply =
                client.DownloadString(
                    string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));

            var captchaResponse = JsonConvert.DeserializeObject<Models.CaptchaResponse>(reply);

            if (!captchaResponse.Success)
                TempData["Message"] = "Lütfen güvenliği doğrulayınız.";
            else
                TempData["Message"] = "Güvenlik başarıyla doğrulanmıştır.";
            return RedirectToAction("Index");
        }
        // GET: Users/Create
        public ActionResult Create()
        {
            Start();
            if (this.UserCheckBasic()) return RedirectToAction("Index","Home");
            CaparKurye.Page page = db.Pages.Where(a => a.pageName.Equals("hizmetsozlesmesi")).FirstOrDefault();
            ViewBag.ServiceAgreement = page.content;
            return View();
        }

        // GET: Users/Login
        public ActionResult Login()
        {
            Start();
            if (this.UserCheckBasic()) return RedirectToAction("Index", "Home");
            return View();
        }

        public void Start()
        {
            ViewBag.notifications = db.Notifications.ToList();
            ViewBag.settings = db.Settings.FirstOrDefault();
        }

        // GET: Users/Profile
        public ActionResult Profile()
        {
            Start();
            if (!this.UserCheckBasic()) return RedirectToAction("Login");
            int userID = Convert.ToInt32(Session["userID"]);
            var obj = db.Users.Where(a => a.userID.Equals(userID)).FirstOrDefault();
            return View(obj);
        }

        // GET: Users/Logout
        public ActionResult Logout()
        {
            if (!this.UserCheckBasic()) return RedirectToAction("Login");
            Session["userID"] = null;
            Session["userAdSoyad"] = null;
            return RedirectToAction("Index", "Home");
        }

        // GET: Users/ForgotPassword
        public ActionResult ForgotPassword()
        {
            Start();
            if (this.UserCheckBasic()) return RedirectToAction("Index", "Home");
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult ForgotPassword([Bind(Include = "email")] Users users)
        {
            if (this.UserCheck()) return Json( new { code=400, message="Zaten üye girişi yapılmış"});

            var obj = db.Users.Where(a => a.email.Equals(users.email)).FirstOrDefault();

            if (obj != null)
            {
                var response = Request["g-recaptcha-response"];
                const string secret = "6LfTbj8UAAAAADzja8Xofe0RVKSrFJd3bt5nQkua";

                var client = new WebClient();
                var reply =
                    client.DownloadString(
                        string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));

                var captchaResponse = JsonConvert.DeserializeObject<Models.CaptchaResponse>(reply);

                if (!captchaResponse.Success)
                    return Json(new { code = 400, message = "Lütfen güvenliği doğrulayınız." });

                sendForgotPasswordMail(obj);
                return Json(new { code = 200, url=Url.Action("Users","Login"), message = "Şifre bilgileri email adresinize başarıyla gönderildi." });
            }
            else
            {
                return Json(new { code = 400, message = "Bu email adresine kayıtlı kullanıcı bulunamadı." });
            }
        }

        // POST: Users/Login
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Login([Bind(Include = "email,password")] Users users)
        {
            var obj = db.Users.Where(a => a.email.Equals(users.email) && a.password.Equals(users.password)).FirstOrDefault();
            if (obj!=null)
            {
                Session["userID"] = obj.userID;
                Session["userAdSoyad"] = obj.nameSurname;
                return RedirectToAction("RequestCourier", "Home");
            }
            else
            {
                TempData["Message"] = "Kullanıcı Adı veya Şifre Yanlış";
                return RedirectToAction("Login");
            }
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult Create([Bind(Include = "type,taxAdministration,taxNumber,TCNumber,authorizedNameSurname,companyAddress,phone,email,password,nameSurname")] Users users)
        {
            var obj = db.Users.Where(a => a.email.Equals(users.email)).FirstOrDefault();
            if (obj == null)
            {
                users.phone = users.phone.Trim();
                users.email = users.email.Trim();
                users.password = users.password.Trim();
                users.nameSurname = users.nameSurname.Trim();
                users.date = DateTime.Now.AddHours(11);
                if (users.type == "individual")
                {
                    users.taxAdministration = "";
                    users.taxNumber = null;
                    users.companyAddress = "";
                    users.authorizedNameSurname = "";
                    users.TCNumber = users.TCNumber.Trim();
                }
                else if (users.type == "corporate")
                {
                    users.taxAdministration = users.taxAdministration.Trim();
                    users.authorizedNameSurname = users.authorizedNameSurname.Trim();
                    users.companyAddress = users.companyAddress.Trim();
                    users.taxNumber = users.taxNumber.Trim();
                    users.TCNumber = null;
                }
                if (ModelState.IsValid)
                {
                    var response = Request["g-recaptcha-response"];
                    const string secret = "6LfTbj8UAAAAADzja8Xofe0RVKSrFJd3bt5nQkua";

                    var client = new WebClient();
                    var reply =
                        client.DownloadString(
                            string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));

                    var captchaResponse = JsonConvert.DeserializeObject<Models.CaptchaResponse>(reply);

                    if (!captchaResponse.Success)
                    return Json(new { code = 400, message = "Lütfen güvenliği doğrulayınız." });

                    db.Users.Add(users);
                    db.SaveChanges();
                    Session["userID"] = users.userID;
                    Session["userAdSoyad"] = users.nameSurname;
                    return Json(new { code=200, url=Url.Action("RequestCourier", "Home"), message="Üye Kaydı Başarıyla Tamamlandı"});
                }
                else
                {
                    return Json(new { code = 400, message = "Eksik ya da Hatalı Giriş Yaptınız." });
                }
            } else {
                return Json(new { code = 400, message = "Email Kullanılıyor" });
            }
        }
        
        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "userID,type,taxAdministration,taxNumber,TCNumber,authorizedNameSurname,companyAddress,phone,email,password,nameSurname,date")] Users users)
        {
            int userID = Convert.ToInt32(Session["UserID"]);
            var obj = db.Users.Where(a => a.email.Equals(users.email) && a.userID!= userID).FirstOrDefault();
            if (obj == null)
            {
                Users user = db.Users.Find(userID);
                if (user.type == "individual")
                {
                    user.TCNumber = users.TCNumber.Trim();
                    user.taxNumber = null;
                    user.taxAdministration = null;
                    user.authorizedNameSurname = null;
                    user.companyAddress = null;
                }
                else if (user.type == "corporate")
                {
                    user.TCNumber = null;
                    user.taxNumber = users.taxNumber.Trim();
                    user.taxAdministration = users.taxAdministration.Trim();
                    user.authorizedNameSurname = users.authorizedNameSurname.Trim();
                    user.companyAddress = users.companyAddress.Trim();
                }
                user.userID = userID;
                user.type = user.type;
                user.phone = users.phone.Trim();
                user.email = users.email.Trim();
                user.password = users.password.Trim();
                user.nameSurname = users.nameSurname.Trim();
                if (ModelState.IsValid)
                {
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                    Session["userAdSoyad"] = user.nameSurname;
                    TempData["code"] = 200;
                    TempData["Message"] = "Düzenleme Başarılı";
                    return RedirectToAction("Profile");
                }
                else
                {
                    TempData["code"] = 400;
                    TempData["Message"] = "Düzenleme Başarısız";
                    return RedirectToAction("Profile");
                }
            }
            else
            {
                TempData["code"] = 400;
                TempData["Message"] = "E-Mail Adresi Kullanılıyor";
                return RedirectToAction("Profile");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
