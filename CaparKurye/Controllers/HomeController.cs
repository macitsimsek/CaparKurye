﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Collections.Specialized;
using Newtonsoft.Json;
using System.Web.Script.Serialization;

namespace CaparKurye.Controllers
{
    public class HomeController : Controller
    {
        private AC.AppContext db = new AC.AppContext();

        public bool UserCheck()
        {
            if (Session["userID"] != null && Session["userAdSoyad"] != null)
            {
                TempData["Message"] = null;
                return true;
            }
            else
            {
                TempData["Message"] = "Lütfen Kullanıcı Girişi Yapınız";
                return false;
            }
        }

        public void sendMail(CaparKurye.Models.ContactMail data)
        {
            string recievermail = ConfigurationManager.AppSettings["recievermail"];
            string mailaddress = ConfigurationManager.AppSettings["mailaddress"];
            string username = ConfigurationManager.AppSettings["username"];
            string password = ConfigurationManager.AppSettings["password"];
            string pop3 = ConfigurationManager.AppSettings["smtp"];


            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage(mailaddress, recievermail);

            message.IsBodyHtml = true;
            message.Subject = "Yeni Mesaj - İletişim Formu";
            message.Body = "<p>Çapar Kurye web sitesinden gelen mail aşağıdaki gibidir.</p>"
                + "<b>Mesaj İçeriği: </b>" + data.message + "<br><br>"
                + "<b>İletişim Bilgileri</b><br>"
                + "<b>Adı Soyadı: </b>" + data.nameSurname + "<br>"
                + "<b>E-posta: </b>" + data.email + "<br>"
                + "<b>Telefon Numarası: </b>" + data.phone + "<br>";
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Host = pop3;
            smtp.Port = 587;

            System.Net.Mail.MailAddress rplyadres = new System.Net.Mail.MailAddress(data.email);
            message.ReplyTo = rplyadres;

            smtp.Credentials = new System.Net.NetworkCredential(username, password);
            smtp.Send(message);
        }

        public void courierRequestedMail(int id)
        {
            string recievermail = ConfigurationManager.AppSettings["recievermail"];
            string mailaddress = ConfigurationManager.AppSettings["mailaddress"];
            string username = ConfigurationManager.AppSettings["username"];
            string password = ConfigurationManager.AppSettings["password"];
            string pop3 = ConfigurationManager.AppSettings["smtp"];
            string domain = ConfigurationManager.AppSettings["domain"];


            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage(mailaddress, recievermail);

            message.IsBodyHtml = true;
            message.Subject = "Yeni Kurye Siparişi";
            message.Body = "<p>Çapar Kurye web sitesi üzerinden Kurye Siparişi gelmiştir</p>"
                + "<a href='" + domain + Url.Action("Details","Couriers",new { id=id}) + "'>Kurye İçeriğini Görüntülemek için Tıklayınız </a><br><br>";
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Host = pop3;
            smtp.Port = 587;

            smtp.Credentials = new System.Net.NetworkCredential(username, password);
            smtp.Send(message);
        }

        public void sendMailHumanResources(CaparKurye.Models.HumanResourcesMail data, HttpPostedFileBase file)
        {
            string recievermail = ConfigurationManager.AppSettings["recievermail"];
            string mailaddress = ConfigurationManager.AppSettings["mailaddress"];
            string username = ConfigurationManager.AppSettings["username"];
            string password = ConfigurationManager.AppSettings["password"];
            string pop3 = ConfigurationManager.AppSettings["smtp"];


            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage(mailaddress, recievermail);

            message.IsBodyHtml = true;
            message.Subject = "Yeni Mesaj - İnsan Kaynakları Başvuru Formu";
            message.Body = "<p>Çapar Kurye web sitesinden gelen mail aşağıdaki gibidir.</p>"
                + "<b>Ad Soyad: </b>" + data.nameSurname + "<br><br>"
                + "<b>Cinsiyet: </b>" + data.gender + "<br><br>"
                + "<b>Pozisyon: </b>" + data.position + "<br><br>"
                + "<b>Telefon: </b>" + data.phone + "<br><br>"
                + "<b>Sigara Durumu: </b>" + data.smoking + "<br><br>"
                + "<b>Medeni Hali: </b>" + data.status + "<br><br>"
                + "<b>TC Kimlik Numarası: </b>" + data.TCNumber + "<br><br>"
                + "<b>Askerlik Durumu: </b>" + data.military + "<br><br>"
                + "<b>Eğitim Durumu: </b>" + data.education + "<br><br>"
                + "<b>Ehliyet: </b>" + data.driverLicance + "<br><br>"
                + "<b>Doğum Yeri: </b>" + data.birthplace + "<br><br>"
                + "<b>Doğum Tarihi: </b>" + data.birthplace + "<br><br>"
                + "<b>Email: </b>" + data.email + "<br><br>";
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Host = pop3;
            smtp.Port = 587;
            if (file != null)
            {
                if (file.ContentLength > 0)
                {
                    string _FileName = Path.GetFileNameWithoutExtension(file.FileName) + "-" + DateTime.Now.AddHours(11).ToString("MM-dd-yyyy-hh-mm-ss") + Path.GetExtension(file.FileName);
                    string _path = Path.Combine(Server.MapPath("~/public/MailAttachments"), _FileName);
                    file.SaveAs(_path);
                    System.Net.Mail.Attachment attachment;
                    attachment = new System.Net.Mail.Attachment(_path);
                    message.Attachments.Add(attachment);
                    message.Body += "CV Ektedir.";
                }
            }

            System.Net.Mail.MailAddress rplyadres = new System.Net.Mail.MailAddress(data.email);
            message.ReplyTo = rplyadres;

            smtp.Credentials = new System.Net.NetworkCredential(username, password);
            smtp.Send(message);
        }

        public void Start()
        {
            ViewBag.notifications = db.Notifications.ToList();
            ViewBag.settings = db.Settings.FirstOrDefault();
        }

        public ActionResult Index()
        {
            Start();
            List<News> newsList = db.News.ToList();
            ViewBag.news = newsList;
            List<Sliders> sliderList = db.Sliders.OrderByDescending(z=>z.order).ToList();
            ViewBag.sliders = sliderList;
            return View();
        }

        [HttpPost]
        public ActionResult reRequest([Bind(Include = "courierID")] Couriers couriers)
        {

            if (!this.UserCheck()) return RedirectToAction("Login", "Users", new { area = "" });
            int userID = Convert.ToInt32(Session["userID"]);
            var courier = db.Couriers.Where(x => x.Users.userID.Equals(userID) && x.courierID.Equals(couriers.courierID)).Select(a => new
            {
                a.courierID,
                a.phone,
                a.nameSurname,
                a.amountOfPost,
                a.contentOfPost,
                a.count,
                a.date,
                a.description,
                a.fromAddress,
                a.fromDistrict,
                a.post,
                a.postType,
                a.tcNumTaxNum,
                CourierRecievers = a.CourierRecievers.Select(s => new { s.nameSurname, s.phone, s.TcNumTaxNum, s.toAddress, s.toDistrict })
            });

            var serializer = new JavaScriptSerializer();
            TempData["Courier"] = serializer.Serialize(courier);
            return RedirectToAction("RequestCourier", "Home");
        }

        public ActionResult NewsDetail(int? id)
        {
            Start();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            List<News> allNews = db.News.ToList();
            int next = allNews.OrderBy(z => z.newsID).Where(x => x.newsID > id).Select(x => x.newsID).FirstOrDefault();
            int previous = allNews.OrderBy(z => z.newsID).Where(x => x.newsID < id).Select(x => x.newsID).FirstOrDefault();
            ViewBag.next = next;
            ViewBag.previous = previous;
            CaparKurye.Page page = db.Pages.Where(a => a.pageName.Equals("haberler")).FirstOrDefault();
            ViewBag.page = page;
            return View(news);
        }

        public ActionResult RequestCourier()
        {
            Start();
            if (!this.UserCheck()) return RedirectToAction("Login", "Users", new { area = "" });

            CaparKurye.Page page = db.Pages.Where(a => a.pageName.Equals("hizmetsozlesmesi")).FirstOrDefault();
            ViewBag.ServiceAgreement = page.content;

            ViewBag.bankAccounts = db.BankAccounts.ToList();
            return View();
        }

        public ActionResult LastPayments()
        {
            Start();
            if (!this.UserCheck()) return RedirectToAction("Login", "Users", new { area = "" });
            int userID = Convert.ToInt32(Session["userID"]);
            List<Couriers> lastPayments = db.Couriers.OrderByDescending(c=>c.courierID).Where(x=>x.Users.userID.Equals(userID)).ToList();
            return View(lastPayments);
        }

        [HttpPost]
        public JsonResult getPayments(int? id)
        {
            if (!this.UserCheck()) return Json(new { code = 400, message = "Üye Girişi Yapınız" });
            int userID = Convert.ToInt32(Session["userID"]);
            var lastPayments = db.Couriers.Where(x => x.courierID==id)
                .Select(a =>new {
                    Payments = new { a.Payments.paymentType, a.Payments.tax, a.Payments.totalCost, a.Payments.cost, a.Payments.BankAccounts.bankName},
                    a.courierID, a.phone,a.nameSurname,a.amountOfPost,a.contentOfPost,a.count,a.date,a.description,a.fromAddress,a.fromDistrict,a.post,a.postType,a.tcNumTaxNum,
                    CourierRecievers = a.CourierRecievers.Select(s => new { s.nameSurname, s.phone, s.TcNumTaxNum, s.toAddress, s.toDistrict})
                }) ;
            return Json(lastPayments);
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult RequestCourier(Couriers couriers,Payments payments,CreditCards creditcard)
        {
            if (!this.UserCheck()) return Json(new { code = 400, message = "Üye Girişi Yapınız." });
            //TODO CREDIT CARD ADD
            payments.creditCardID = null;
            db.Payments.Add(payments);
            db.SaveChanges();
            payments.paymentStatus = "Ödenmedi";
            couriers.paymentID = payments.paymentID;
            couriers.Payments = payments;
            couriers.date = DateTime.Now.AddHours(11);
            couriers.userID = Convert.ToInt32(Session["userID"]);
            couriers.Users = db.Users.Find(couriers.userID);
            db.Couriers.Add(couriers);
            db.SaveChanges();
            //TELESKOP SİPARİŞ EKLEME
            PostTeleskop(couriers);
            //BİLGİ MAİLİ GÖNDERME
            courierRequestedMail(couriers.courierID);
            return Json(new { code = 200,url= Url.Action("LastPayments", "Home", null, Request.Url.Scheme), message = "Kurye Talebi Başarıyla Alındı." });
        }


        public ActionResult PostTeleskop(Couriers couriers)
        {
            try
            {
                string customer_name = couriers.nameSurname.Trim();
                string city = couriers.fromDistrict.Split(',')[0].Trim();
                string district = couriers.fromDistrict.Split(',')[1].Trim();
                string address = couriers.fromAddress.Trim();
                string description = couriers.description.Trim();
                DateTime date = DateTime.Now.AddHours(11);
                string scheduled_time = date.Day.ToString() + "/" + date.Month.ToString() + "/" + date.Year.ToString() + " " + date.Hour.ToString() + ":" + date.Minute.ToString();

                NameValueCollection kullanicilar = new NameValueCollection();

                kullanicilar.Add("customer_name", customer_name);
                kullanicilar.Add("address", address);
                kullanicilar.Add("city", city);
                kullanicilar.Add("district", district);
                kullanicilar.Add("description", description);
                kullanicilar.Add("scheduled_time", scheduled_time);
                string remoteUrl = "https://uygulama.teleskop.co/webhook/create-order?access_token=c48aa624-d4f5-11e7-9a0b-06de82716f52";
                var client = new WebClient();
                client.Encoding = System.Text.Encoding.UTF8;
                var reply =
                    client.UploadValues(remoteUrl, "POST", kullanicilar);
                return Json(new { reply = reply }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { reply = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult AddRecievers(int courierID)
        {
            return new JsonResult();
        }

        public ActionResult About()
        {
            Start();
            CaparKurye.Page page = db.Pages.Where(a => a.pageName.Equals("hakkimizda")).FirstOrDefault();
            ViewBag.page = page;

            return View();
        }

        public ActionResult Services()
        {
            Start();
            CaparKurye.Page page = db.Pages.Where(a => a.pageName.Equals("hizmetler")).FirstOrDefault();
            ViewBag.page = page;

            List<CaparKurye.Services> list = db.Services.ToList();

            return View(list);
        }
        public ActionResult News()
        {
            Start();
            CaparKurye.Page page = db.Pages.Where(a => a.pageName.Equals("haberler")).FirstOrDefault();
            ViewBag.page = page;

            List<CaparKurye.News> list = db.News.ToList();

            return View(list);
        }

        public ActionResult ServiceAgreement()
        {
            Start();
            CaparKurye.Page page = db.Pages.Where(a => a.pageName.Equals("hizmetsozlesmesi")).FirstOrDefault();
            ViewBag.page = page;

            return View();
        }

        public ActionResult Files()
        {
            Start();

            List<CaparKurye.Files> files = db.Files.ToList();

            return View(files);
        }
        public ActionResult HumanResources()
        {
            Start();

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult HumanResources([Bind(Include = "position,TCNumber,nameSurname,gender,status,birthdate,birthplace,education,military,driverLicance,smoking,phone,email")] CaparKurye.Models.HumanResourcesMail human, HttpPostedFileBase file)
        {
            sendMailHumanResources(human,file);

            TempData["Message"] = "Başarıyla Gönderildi.";
            return RedirectToAction("HumanResources");
        }

        public ActionResult Contact()
        {
            Start();

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Contact([Bind(Include = "nameSurname,phone,email,message")] CaparKurye.Models.ContactMail contact)
        {
            sendMail(contact);

            TempData["Message"] = "Başarıyla Gönderildi.";
            return RedirectToAction("Contact");
        }

        public ActionResult Faq()
        {
            Start();
            CaparKurye.Page page = db.Pages.Where(a => a.pageName.Equals("sss")).FirstOrDefault();
            ViewBag.page = page;

            Dictionary<string, List<CaparKurye.Faqs>> genel = db.Faqs.GroupBy(o => o.category).ToDictionary(g => g.Key, g => g.ToList());

            return View(genel);
        }

    }
}