﻿<%@ WebHandler Language="C#" Class="Upload" %>

using System;
using System.Web;



public class Upload : System.Web.IHttpHandler {


    public void ProcessRequest(HttpContext context)
    {

        string fileName = "";
        string filePath = context.Server.MapPath("../") + "\\dokumanlar\\upload\\";
        
        if (context.Request.Files.Count > 0)
        {
            HttpFileCollection files = context.Request.Files;
            foreach (string key in files)
            {
                HttpPostedFile file = files[key];
                fileName = DateTime.Now.AddHours(11).ToString("yyyyMMddHHmmss") + "_" + file.FileName;
                file.SaveAs(filePath + fileName);
            }
        }
		
		if (context.Request.QueryString["source"] == null)
		{
            context.Response.ContentType = "text/plain";
            context.Response.Write(fileName);
		}
		else
		{
		    string CKEditorFuncNum = context.Request["CKEditorFuncNum"];
            string url = "dokumanlar/upload/" + fileName; // resimlerin yüklendiği klasör ve yolu alıyoruz..
		    context.Response.Write("<script>window.parent.CKEDITOR.tools.callFunction(" + CKEditorFuncNum + ", \"" + url + "\");</script>");
		}
		context.Response.End();
		
    }
    
public bool IsReusable {
get {
return false;
}
}
}
