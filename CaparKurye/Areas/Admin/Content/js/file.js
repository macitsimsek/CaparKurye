﻿var dosyalar = [];
var callbackfonksiyon = 0;

var buf = new ArrayBuffer();

var tmp = 0;

function dosyaUyarisiVer() {
    LoaderShow();
}

function dosyaUyarisiKapat() {
    LoaderHide();
}

function dosyaData(i) {
    var dosya = {};
    dosya.veri = dosyalar[i].veri;
    dosya.dosyaadi = dosyalar[i].dosyaadi;
    dosya.dosyaturu = dosyalar[i].dosyaturu; // tablodan dropdowndan gelecek
    return dosya;
}
var geriSayim = 0;
function dosyaSecildi(inp, dosyaSecildiCallback) {
    if (inp.files.length > 0) {
        dosyaUyarisiVer();
    }
    geriSayim = inp.files.length;

    tmp = inp;
    for (var i = 0; i < inp.files.length; i++) {
        var reader = new FileReader();
        reader.onload = onloadUret(tmp.files[i].name, tmp.files[i].type, dosyaSecildiCallback);
        reader.readAsArrayBuffer(tmp.files[i]);
    }
}


function dataSecildi(inp, dosyaSecildiCallback) {
    dosyaSecildi(inp, dosyaSecildiCallback);
}


function onloadUret(name, type, dosyaSecildiCallbackTmp) {
    return function (e) {
        var dosya = {};
        dosya.veri = btoa(ab2str(e.target.result));
        dosya.dosyaadi = name;
        dosya.dosyaturu = type;
        dosya.kayitadi = "";
        $(".divKayitIcinBekleyenDosyalar").append($("<div style='display:inline-block;padding:5px;margin:10px; border:1px solid #000;background-color:red;font-weight:bold;color:white;'>" + name + "</div>"))
        var indis = dosyalar.push(dosya) - 1;
        geriSayim--;
        if (geriSayim <= 0) {
            dosyaUyarisiKapat();
        }

        dosyaSecildiCallbackTmp(dosya, indis);
    };
}



function ab2str(buf) {
    var arr = new Uint8Array(buf);
    var data = "";
    for (var i = 0; i < arr.length; i++) {
        data += String.fromCharCode(arr[i]);
    }
    return data;
}

var loaderShowSayac = 0;
function LoaderShow() {
    var loaderdiv = "<div class='yukleniyor' style='position:fixed; top:0; left:0; right:0; bottom:0; z-index:9999; background-color:#000; opacity:0.5; display:none;'></div>" +
    "<div class='yukleniyor' style='position:fixed; top:40%; left:10%; right:10%; bottom:40%; z-index:10000;  background-color:#000; border:2px solid #e2e2e2; border-radius:12px; opacity:1 !important; color:#FFF; text-align:center; display:none;'></div>" +
    "<div class='yukleniyor' style='position:fixed; top:48%; left:11%; right:11%; bottom:48%; z-index:10001;  background-color:#000; opacity:1 !important; color:#FFF; vertical-align:middle; text-align:center; display:none;' >Yükleniyor...</div>";
    $("body").append(loaderdiv);

    $(".yukleniyor").show();
    loaderShowSayac++;
}

function LoaderHide() {
    //$(".loading").removeAttr("class").attr("hide","true");
    setTimeout(function () {
        loaderShowSayac--;
        if (loaderShowSayac >= 0) {
            $(".yukleniyor").remove();
        }
    }, 100);
}