﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CaparKurye.AC;

namespace CaparKurye.Areas.Admin.Controllers
{
    public class CouriersController : Controller
    {
        private AC.AppContext db = new AC.AppContext();

        public bool UserCheck()
        {
            if (Session["adminID"] != null && Session["adminName"] != null)
            {
                TempData["Message"] = null;
                return true;
            }
            else
            {
                TempData["Message"] = "Lütfen Admin Girişi Yapınız";
                return false;
            }
        }

        // GET: Couriers
        public ActionResult Index()
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            var couriers = db.Couriers.Include(c => c.Payments).Include(c => c.Users).OrderByDescending(x=>x.courierID);
            return View(couriers.ToList());
        }

        // GET: Couriers/Details/5
        public ActionResult Details(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Couriers couriers = db.Couriers.Find(id);
            if (couriers == null)
            {
                return HttpNotFound();
            }
            ViewBag.paymentTypes = db.Payments.ToList();
            ViewBag.bankAccounts = db.BankAccounts.ToList();

            return View(couriers);
        }

        [HttpPost]
        public ActionResult GetCouriersByDate(string firstDate, string secondDate)
        {
            //if (!this.UserCheck()) return Json(new { code = 400, message = "Üye Girişi Yapınız" }, JsonRequestBehavior.AllowGet);
            if (firstDate == "")
            {
                firstDate = "2000-01-01";
            }
            if (secondDate == "")
            {
                secondDate = "2000-01-01";
            }
            DateTime firstDateConvert = DateTime.ParseExact(firstDate, "yyyy-MM-dd",
                                       System.Globalization.CultureInfo.InvariantCulture);
            DateTime secondDateConvert = DateTime.ParseExact(secondDate, "yyyy-MM-dd",
                                       System.Globalization.CultureInfo.InvariantCulture);

            var couriers = db.Couriers.OrderByDescending(x=>x.courierID).Where(a => (a.date >= firstDateConvert && a.date <= secondDateConvert)).Select(x=>new { x.courierID,x.count,x.date,x.post,x.postType,x.Payments.paymentType,x.status,x.Payments.totalCost,x.Payments.cost,x.Payments.tax,x.Users.userID});

            return Json(new { code = 200, data = new { couriers = couriers } }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ChangeStatus(int courierID,string status)
        {
            if (!this.UserCheck()) return Json(new {code =400, message ="Admin Girişi Yapınız" });
            Couriers couriers = db.Couriers.Find(courierID);
            if (couriers == null)
            {
                return Json(new { code = 400, message = "Kurye Bulunamadı" });
            }
            int id = Convert.ToInt32(Session["adminID"].ToString());
            var obj2 = db.Admins.Where(a => a.adminID.Equals(id)).FirstOrDefault();

            couriers.status = status;
            db.Entry(couriers).State = EntityState.Modified;
            db.SaveChanges();
            return Json(new { code = 200, message = "Durum Başarıyla Güncellendi" });
        }

        [HttpPost]
        public JsonResult ChangePaymentStatus(int courierID, string paymentStatus)
        {
            if (!this.UserCheck()) return Json(new { code = 400, message = "Admin Girişi Yapınız" });
            Couriers couriers = db.Couriers.Find(courierID);
            if (couriers == null)
            {
                return Json(new { code = 400, message = "Kurye Bulunamadı" });
            }
            int id = Convert.ToInt32(Session["adminID"].ToString());
            var obj2 = db.Admins.Where(a => a.adminID.Equals(id)).FirstOrDefault();
            if (obj2.adminType == "Admin" || obj2.adminType == "Muhasebe")
            {
                couriers.Payments.paymentStatus = paymentStatus;
                db.Entry(couriers).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { code = 200, message = "Durum Başarıyla Güncellendi" });
            }
            else
            {
                return Json(new { code = 400, message = "Yetkisiz Admin Tipi" });
            }
        }
        
        [HttpPost]
        public JsonResult SetCostandPaymentType(int courierID, string paymentType, string totalCost, string cost, string tax,string accountID)
        {
            if (!this.UserCheck()) return Json(new { code = 400, message = "Admin Girişi Yapınız" });
            Couriers couriers = db.Couriers.Find(courierID);
            if (couriers == null)
            {
                return Json(new { code = 400, message = "Kurye Bulunamadı" });
            }
            int id = Convert.ToInt32(Session["adminID"].ToString());
            var obj2 = db.Admins.Where(a => a.adminID.Equals(id)).FirstOrDefault();
            if (obj2.adminType == "Admin" || obj2.adminType == "Muhasebe")
            {
                couriers.Payments.paymentType = paymentType;
                couriers.Payments.totalCost = Convert.ToDecimal(totalCost);
                couriers.Payments.cost = Convert.ToDecimal(cost);
                couriers.Payments.tax = Convert.ToDecimal(tax);
                if(accountID!="")couriers.Payments.accountID = Convert.ToInt32(accountID);
                db.Entry(couriers).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { code = 200, message = "Teklif ve Ücret Başarıyla Güncellendi" });
            }
            else
            {
                return Json(new { code = 400, message = "Yetkisiz Admin Tipi" });
            }
        }

        // GET: Couriers/Create
        public ActionResult Create()
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            ViewBag.paymentID = new SelectList(db.Payments, "paymentID", "paymentType");
            ViewBag.userID = new SelectList(db.Users, "userID", "type");
            return View();
        }

        // POST: Couriers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "courierID,nameSurname,phone,tcNumTaxNum,fromDistrict,fromAddress,post,postType,count,description,amountOfPost,contentOfPost,date,userID,paymentID,status")] Couriers couriers)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            if (ModelState.IsValid)
            {
                db.Couriers.Add(couriers);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.paymentID = new SelectList(db.Payments, "paymentID", "paymentType", couriers.paymentID);
            ViewBag.userID = new SelectList(db.Users, "userID", "type", couriers.userID);
            return View(couriers);
        }

        // GET: Couriers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Couriers couriers = db.Couriers.Find(id);
            if (couriers == null)
            {
                return HttpNotFound();
            }
            ViewBag.paymentID = new SelectList(db.Payments, "paymentID", "paymentType", couriers.paymentID);
            ViewBag.userID = new SelectList(db.Users, "userID", "type", couriers.userID);
            return View(couriers);
        }

        // POST: Couriers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "courierID,nameSurname,phone,tcNumTaxNum,fromDistrict,fromAddress,post,postType,count,description,amountOfPost,contentOfPost,date,userID,paymentID,status")] Couriers couriers)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            if (ModelState.IsValid)
            {
                db.Entry(couriers).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.paymentID = new SelectList(db.Payments, "paymentID", "paymentType", couriers.paymentID);
            ViewBag.userID = new SelectList(db.Users, "userID", "type", couriers.userID);
            return View(couriers);
        }

        // GET: Couriers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Couriers couriers = db.Couriers.Find(id);
            if (couriers == null)
            {
                return HttpNotFound();
            }
            return View(couriers);
        }

        // POST: Couriers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            Couriers couriers = db.Couriers.Find(id);
            if (couriers.CourierRecievers.Count > 0)
            {
                List<CourierRecievers> reciever = couriers.CourierRecievers.ToList();
                foreach (var item in reciever)
                {
                    db.CourierRecievers.Remove(item);
                }
            }
            if (couriers.Payments != null)
            {
                db.Payments.Remove(couriers.Payments);
            }
            db.Couriers.Remove(couriers);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
