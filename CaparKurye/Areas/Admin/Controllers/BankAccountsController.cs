﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CaparKurye.AC;
using System.IO;

namespace CaparKurye.Areas.Admin.Controllers
{
    public class BankAccountsController : Controller
    {
        private AC.AppContext db = new AC.AppContext();

        // GET: BankAccounts
        public ActionResult Index()
        {
            var bankAccounts = db.BankAccounts.ToList();
            return View(bankAccounts);
        }

        // GET: BankAccounts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankAccounts bankAccounts = db.BankAccounts.Find(id);
            if (bankAccounts == null)
            {
                return HttpNotFound();
            }
            return View(bankAccounts);
        }

        // GET: BankAccounts/Create
        public ActionResult Create()
        {
            ViewBag.pictureID = new SelectList(db.Pictures, "pictureID", "name");
            return View();
        }

        // POST: BankAccounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "accountID,bankName,ibanCode,branchCode,accountCode,pictureID")] BankAccounts bankAccounts, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    if (file.ContentLength > 0)
                    {
                        string _FileName = Path.GetFileNameWithoutExtension(file.FileName) + "-" + DateTime.Now.AddHours(11).ToString("MM-dd-yyyy-hh-mm-ss") + Path.GetExtension(file.FileName);
                        string _path = Path.Combine(Server.MapPath("~/public/UploadedFiles"), _FileName);
                        file.SaveAs(_path);
                        Pictures pictures = new Pictures();
                        pictures.title = file.FileName;
                        pictures.name = file.FileName;
                        db.Pictures.Add(pictures);
                        db.SaveChanges();
                        bankAccounts.pictureID = pictures.pictureID;
                    }
                }
                db.BankAccounts.Add(bankAccounts);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.pictureID = new SelectList(db.Pictures, "pictureID", "title", bankAccounts.pictureID);
            return View(bankAccounts);
        }

        // GET: BankAccounts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankAccounts bankAccounts = db.BankAccounts.Find(id);
            if (bankAccounts == null)
            {
                return HttpNotFound();
            }
            ViewBag.pictureID = new SelectList(db.Pictures, "pictureID", "title", bankAccounts.pictureID);
            return View(bankAccounts);
        }

        // POST: BankAccounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "accountID,bankName,ibanCode,branchCode,accountCode,pictureID")] BankAccounts bankAccounts, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    if (file.ContentLength > 0)
                    {
                        string _FileName = Path.GetFileNameWithoutExtension(file.FileName) + "-" + DateTime.Now.AddHours(11).ToString("MM-dd-yyyy-hh-mm-ss") + Path.GetExtension(file.FileName);
                        string _path = Path.Combine(Server.MapPath("~/public/UploadedFiles"), _FileName);
                        file.SaveAs(_path);
                        Pictures pictures = new Pictures();
                        pictures.title = file.FileName;
                        pictures.name = file.FileName;
                        db.Pictures.Add(pictures);
                        db.SaveChanges();
                        bankAccounts.pictureID = pictures.pictureID;
                    }
                }
                BankAccounts bnk = db.BankAccounts.Find(bankAccounts.accountID);
                bnk.accountCode = bankAccounts.accountCode;
                bnk.bankName = bankAccounts.bankName;
                bnk.ibanCode = bankAccounts.ibanCode;
                bnk.branchCode = bankAccounts.branchCode;
                bnk.pictureID = bankAccounts.pictureID;
                db.Entry(bnk).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.pictureID = new SelectList(db.Pictures, "pictureID", "title", bankAccounts.pictureID);
            return View(bankAccounts);
        }

        // GET: BankAccounts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankAccounts bankAccounts = db.BankAccounts.Find(id);
            if (bankAccounts == null)
            {
                return HttpNotFound();
            }
            return View(bankAccounts);
        }

        // POST: BankAccounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BankAccounts bankAccounts = db.BankAccounts.Find(id);
            db.BankAccounts.Remove(bankAccounts);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
