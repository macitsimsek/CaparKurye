﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CaparKurye.AC;

namespace CaparKurye.Areas.Admin.Controllers
{
    public class SettingsController : Controller
    {
        private AC.AppContext db = new AC.AppContext();

        public bool UserCheck()
        {
            if (Session["adminID"] != null && Session["adminName"] != null)
            {
                TempData["Message"] = null;
                return true;
            }
            else
            {
                TempData["Message"] = "Lütfen Admin Girişi Yapınız";
                return false;
            }
        }

        // GET: Settings
        public ActionResult Index()
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            return View(db.Settings.ToList());
        }

        // GET: Settings/Details/5
        public ActionResult Details(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Settings settings = db.Settings.Find(id);
            if (settings == null)
            {
                return HttpNotFound();
            }
            return View(settings);
        }

        // GET: Settings/Create
        public ActionResult Create()
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            return View();
        }

        // POST: Settings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "settingID,title,description,keyword,facebook,twitter,linkedin,instagram")] Settings settings)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            if (ModelState.IsValid)
            {
                db.Settings.Add(settings);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(settings);
        }

        // GET: Settings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Settings settings = db.Settings.Find(id);
            if (settings == null)
            {
                return HttpNotFound();
            }
            return View(settings);
        }

        // POST: Settings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "settingID,title,description,keyword,facebook,twitter,linkedin,instagram")] Settings settings)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            if (ModelState.IsValid)
            {
                db.Entry(settings).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(settings);
        }

        // GET: Settings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Settings settings = db.Settings.Find(id);
            if (settings == null)
            {
                return HttpNotFound();
            }
            return View(settings);
        }

        // POST: Settings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Settings settings = db.Settings.Find(id);
            db.Settings.Remove(settings);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
