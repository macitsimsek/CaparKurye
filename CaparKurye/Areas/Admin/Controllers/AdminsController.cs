﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CaparKurye.AC;

namespace CaparKurye.Areas.Admin.Controllers
{
    public class AdminsController : Controller
    {
        private AC.AppContext db = new AC.AppContext();

        public bool UserCheckBasic()
        {
            if (Session["adminID"] != null && Session["adminName"] != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool UserCheck()
        {
            if (Session["adminID"] != null && Session["adminName"] != null)
            {
                TempData["Message"] = null;
                return true;
            }
            else
            {
                TempData["Message"] = "Lütfen Admin Girişi Yapınız";
                return false;
            }
        }

        // GET: Admins
        public ActionResult Index()
        {
            if (!this.UserCheck()) return RedirectToAction("Dashboard");

            int id = Convert.ToInt32(Session["adminID"]);
            return View(db.Admins.Where(x=>x.adminID!=id).ToList());
        }

        // GET: Login
        public ActionResult Login()
        {
            if (this.UserCheckBasic()) return RedirectToAction("Dashboard");

            return View();
        }

        // GET: Dashboard
        public ActionResult Dashboard()
        {
            if (!this.UserCheck()) return RedirectToAction("Login");
            int id = Convert.ToInt32(Session["adminID"].ToString());
            var obj2 = db.Admins.Where(a => a.adminID.Equals(id)).FirstOrDefault();
            if (obj2.adminType != "Admin") return RedirectToAction("Index","Couriers");
            ViewBag.individualCount = db.Users.Where(a => a.type.Equals("individual")).Count();
            ViewBag.corporateCount = db.Users.Where(a => a.type.Equals("corporate")).Count();
            List<Couriers> q = db.Couriers.OrderByDescending(x=>x.courierID).ToList();
            ViewBag.courierRequests = q;
            ViewBag.courierCount = q.Count();
            ViewBag.successCount = q.Where(x=>x.status.Equals("Onaylandı")).Count();
            ViewBag.waitingCount = q.Where(x => x.status.Equals("Bekliyor")).Count();
            ViewBag.cancelCount = q.Where(x => x.status.Equals("İptal")).Count();

            ViewBag.paymentSuccessCount = q.Where(x => x.Payments.paymentStatus.Equals("Ödendi")).Count();
            ViewBag.paymentWaitingCount = q.Where(x => x.Payments.paymentStatus.Equals("Ödenmedi")).Count();
            ViewBag.paymentCancelCount = q.Where(x => x.Payments.paymentStatus.Equals("İade")).Count();

            ViewBag.years = db.Couriers.GroupBy(
                p => p.date.Value.Year,
                (key, g) => new {year = key,count = g.Count()}).OrderByDescending(x => x.year).Select(z => z.year).ToList();
            return View(q);
        }

        // GET: Dashboard
        public ActionResult Logout()
        {
            Session["adminID"] = null;
            Session["adminType"] = null;
            Session["adminName"] = null;
            return RedirectToAction("Login");
        }


        // POST: Admins/Login
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login([Bind(Include = "adminName,password")] Admins admins)
        {
            if (this.UserCheckBasic()) return RedirectToAction("Dashboard");

            if (ModelState.IsValid)
            {
                var obj = db.Admins.Where(a => a.adminName.Equals(admins.adminName) && a.password.Equals(admins.password)).FirstOrDefault();
                if (obj != null)
                {
                    Session["adminID"] = obj.adminID.ToString();
                    Session["adminType"] = obj.adminType.ToString();
                    Session["adminName"] = obj.adminName.ToString();
                    return RedirectToAction("Dashboard");
                }
                else
                {
                    TempData["Message"] = "Kullanıcı Adı veya Şifre Hatalı";
                    return RedirectToAction("Login");
                }
            }

            return View();
        }

        // GET: Admins/Details/5
        public ActionResult Details(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Admins admins = db.Admins.Find(id);
            if (admins == null)
            {
                return HttpNotFound();
            }
            return View(admins);
        }

        // GET: Admins/Create
        public ActionResult Create()
        {
            if (!this.UserCheck()) return RedirectToAction("Login");

            return View();
        }

        // GET: Admins/GetCouriersByDate
        [HttpPost]
        public ActionResult GetCouriersByDate(string firstDate, string secondDate)
        {
            if (!this.UserCheck()) return Json(new { code=400, message = "Üye Girişi Yapınız" }, JsonRequestBehavior.AllowGet);
            if (firstDate == "")
            {
                firstDate = "2000-01-01";
            }
            if (secondDate == "")
            {
                secondDate = "2000-01-01";
            }
            DateTime firstDateConvert = DateTime.ParseExact(firstDate+ " 00:00:00", "yyyy-MM-dd HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);
            DateTime secondDateConvert = DateTime.ParseExact(secondDate+ " 23:59:59", "yyyy-MM-dd HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);

            var top3FromDistrict = db.Couriers.Where(a => (a.date >= firstDateConvert && a.date <= secondDateConvert)).GroupBy(
                p => p.fromDistrict,
                (key, g) => new { fromDistrict = key.Trim(), count = g.Count() }).OrderByDescending(x => x.count);

            var couriers = db.Couriers.Where(a => (a.date >= firstDateConvert && a.date <= secondDateConvert)).GroupBy(
                p => p.postType,
                (key, g) => new { postType = key.Trim(), count = g.Count() });

            return Json(new { code = 200, data = new { couriers = couriers, top3FromDistrict = top3FromDistrict } }, JsonRequestBehavior.AllowGet);
        }
        // GET: Admins/GetCouriersByDateAll
        [HttpPost]
        public ActionResult GetCouriersByDateAll(string firstDate, string secondDate)
        {
            if (!this.UserCheck()) return Json(new { code = 400, message = "Üye Girişi Yapınız" }, JsonRequestBehavior.AllowGet);
            if (firstDate == ""){
                firstDate = "2000-01-01";
            }
            if (secondDate == ""){
                secondDate = "2000-01-01";
            }

            DateTime firstDateConvert = DateTime.ParseExact(firstDate + " 00:00:00", "yyyy-MM-dd HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);
            DateTime secondDateConvert = DateTime.ParseExact(secondDate + " 23:59:59", "yyyy-MM-dd HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);


            var couriers = db.Couriers.Where(a => (a.date >= firstDateConvert && a.date <= secondDateConvert)).OrderByDescending(x => x.courierID).Select(x => new { x.postType, x.status, x.Users.userID, x.post, x.count, x.Payments.paymentType, x.Payments.cost, x.Payments.tax, x.Payments.totalCost, x.date, x.courierID, x.Payments.paymentStatus });

            return Json(new { code = 200, data = new { couriers = couriers } }, JsonRequestBehavior.AllowGet);
        }

        // GET: Admins/GetCiroByDate
        [HttpPost]
        public ActionResult GetCiroByDate(string firstDate, string secondDate)
        {
            if (!this.UserCheck()) return Json(new { code = 400, message = "Üye Girişi Yapınız" }, JsonRequestBehavior.AllowGet);
            if (firstDate == "")
            {
                firstDate = "2000-01-01";
            }
            if (secondDate == "")
            {
                secondDate = "2000-01-01";
            }
            DateTime firstDateConvert = DateTime.ParseExact(firstDate + " 00:00:00", "yyyy-MM-dd HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);
            DateTime secondDateConvert = DateTime.ParseExact(secondDate + " 23:59:59", "yyyy-MM-dd HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);

           
            var couriers = db.Couriers.Where(a => (a.date >= firstDateConvert && a.date <= secondDateConvert)).Where(z=>z.Payments.paymentStatus.Equals("Ödendi")).OrderByDescending(x=>x.courierID).Select(x=> new {x.postType , x.status,x.Users.userID,x.post,x.count, x.Payments.paymentType, x.Payments.cost, x.Payments.tax, x.Payments.totalCost,x.date,x.courierID });

            return Json(new { code = 200, data = new { couriers = couriers } }, JsonRequestBehavior.AllowGet);
        }


        // GET: Admins/GetYearlyPayments
        [HttpPost]
        public ActionResult GetYearlyPayments(int year)
        {
            if (!this.UserCheck()) return Json(new { code=400, message = "Üye Girişi Yapınız" }, JsonRequestBehavior.AllowGet);

            var paymentTypes = db.Couriers.GroupBy(
                p => p.Payments.paymentType,
                (key, g) => new { paymentType = key, count = g.Count() });

            var couriers = db.Couriers.Where(x => x.date.Value.Year.Equals(year));

            var PaymentSums = couriers.GroupBy(
                p => p.Payments.paymentType,
                (key, g) => new {
                    paymentType = key,
                    Sum = g.Sum(x=>x.Payments.totalCost)
                });

            var yearlyPayments = couriers.GroupBy(
                p => p.date.Value.Month,
                (key, g) => new { month = key, Payments = g.GroupBy(x => x.Payments.paymentType,
                (key2,g2)=> new { paymentType = key2, count= g2.Count() }) });


            return Json(new { code = 200, data = new { paymentTypes = paymentTypes, yearlyPayments = yearlyPayments, PaymentSums = PaymentSums } }, JsonRequestBehavior.AllowGet);
        }

        // POST: Admins/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "adminName,adminType,password")] Admins admins)
        {
            if (!this.UserCheck()) return RedirectToAction("Login");
            var obj = db.Admins.Where(a => a.adminName.Equals(admins.adminName)).FirstOrDefault();
            if (obj == null)
            {
                if (ModelState.IsValid)
                {
                    admins.date = DateTime.Now.AddHours(11);
                    db.Admins.Add(admins);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            else
            {
                TempData["Message"] = "Admin Adı Kullanılıyor";
            }

            return View(admins);
        }

        // GET: Admins/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Admins admins = db.Admins.Find(id);
            if (admins == null)
            {
                return HttpNotFound();
            }
            return View(admins);
        }

        // POST: Admins/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "adminName,adminType,password")] Admins admins)
        {
            if (!this.UserCheck()) return RedirectToAction("Login");

            if (ModelState.IsValid)
            {
                admins.adminID = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"]);
                int id = Convert.ToInt32(Session["adminID"].ToString());
                var obj2 = db.Admins.Where(a => a.adminID.Equals(id)).FirstOrDefault();
                if (obj2.adminType=="Admin")
                {
                    var obj = db.Admins.Where(a => a.adminName.Equals(admins.adminName)).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.adminName = admins.adminName;
                        obj.adminType = admins.adminType;
                        obj.password = admins.password;
                        db.Entry(obj).State = EntityState.Modified;
                        db.SaveChanges();
                        Session["adminName"] = admins.adminName.ToString();
                    }
                    else
                    {
                        TempData["Message"] = "Admin Adı Kullanılıyor";
                    }
                }
                else
                {
                    TempData["Message"] = "Yetkisiz Admin";
                }
            }
            return View(admins);
        }

        public ActionResult Profile()
        {
            if (!this.UserCheck()) return RedirectToAction("Login");
            int id = Convert.ToInt32(Session["adminID"].ToString());
            Admins admins = db.Admins.Find(id);
            if (admins == null)
            {
                return HttpNotFound();
            }
            return View(admins);
        }

        // POST: Admins/Profile
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Profile([Bind(Include = "adminName,password")] Admins admins)
        {
            if (!this.UserCheck()) return RedirectToAction("Login");

            if (ModelState.IsValid)
            {
                int id = Convert.ToInt32(Session["adminID"]);
                admins.adminID = id;
                var obj = db.Admins.Where(a => a.adminName.Equals(admins.adminName) && a.adminID != admins.adminID).FirstOrDefault();
                if (obj == null)
                {
                    var adm = db.Admins.Where(a => a.adminID == admins.adminID).FirstOrDefault();
                    adm.adminName = admins.adminName;
                    adm.password = admins.password;
                    db.Entry(adm).State = EntityState.Modified;
                    db.SaveChanges();
                    Session["adminName"] = admins.adminName.ToString();
                } else {
                    TempData["Message"] = "Admin Adı Kullanılıyor";
                }
            }
            return View(admins);
        }

        // GET: Admins/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Admins admins = db.Admins.Find(id);
            if (admins == null)
            {
                return HttpNotFound();
            }
            return View(admins);
        }

        // POST: Admins/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login");

            Admins admins = db.Admins.Find(id);
            db.Admins.Remove(admins);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
