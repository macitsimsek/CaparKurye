﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CaparKurye.AC;
using System.IO;

namespace CaparKurye.Areas.Admin.Controllers
{
    public class FilesController : Controller
    {
        private AC.AppContext db = new AC.AppContext();

        public bool UserCheck()
        {
            if (Session["adminID"] != null && Session["adminName"] != null)
            {
                TempData["Message"] = null;
                return true;
            }
            else
            {
                TempData["Message"] = "Lütfen Admin Girişi Yapınız";
                return false;
            }
        }

        // GET: Files
        public ActionResult Index()
        {
            var files = db.Files.Include(f => f.Pictures);
            return View(files.ToList());
        }

        // GET: Files/Details/5
        public ActionResult Details(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Files files = db.Files.Find(id);
            if (files == null)
            {
                return HttpNotFound();
            }
            return View(files);
        }

        // GET: Files/Create
        public ActionResult Create()
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            ViewBag.pictureID = new SelectList(db.Pictures, "pictureID", "name");
            return View();
        }

        // POST: Files/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "fileID,pictureID")] Files files, HttpPostedFileBase file)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    if (file.ContentLength > 0)
                    {
                        string _FileName = Path.GetFileNameWithoutExtension(file.FileName) + "-" + DateTime.Now.AddHours(11).ToString("MM-dd-yyyy-hh-mm-ss") + Path.GetExtension(file.FileName);
                        string _path = Path.Combine(Server.MapPath("~/public/UploadedFiles"), _FileName);
                        file.SaveAs(_path);
                        Pictures pictures = new Pictures();
                        pictures.title = file.FileName;
                        pictures.name = file.FileName;
                        db.Pictures.Add(pictures);
                        db.SaveChanges();
                        files.pictureID = pictures.pictureID;
                    }
                }
                db.Files.Add(files);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.pictureID = new SelectList(db.Pictures, "pictureID", "name", files.pictureID);
            return View(files);
        }

        // GET: Files/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Files files = db.Files.Find(id);
            if (files == null)
            {
                return HttpNotFound();
            }
            ViewBag.pictureID = new SelectList(db.Pictures, "pictureID", "name", files.pictureID);
            return View(files);
        }

        // POST: Files/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "fileID,pictureID")] Files files, HttpPostedFileBase file)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    if (file.ContentLength > 0)
                    {
                        string _FileName = Path.GetFileNameWithoutExtension(file.FileName) + "-" + DateTime.Now.AddHours(11).ToString("MM-dd-yyyy-hh-mm-ss") + Path.GetExtension(file.FileName);
                        string _path = Path.Combine(Server.MapPath("~/public/UploadedFiles"), _FileName);
                        file.SaveAs(_path);
                        Pictures pictures = new Pictures();
                        pictures.title = file.FileName;
                        pictures.name = file.FileName;
                        db.Pictures.Add(pictures);
                        db.SaveChanges();
                        files.pictureID = pictures.pictureID;
                    }
                }
                db.Entry(files).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.pictureID = new SelectList(db.Pictures, "pictureID", "name", files.pictureID);
            return View(files);
        }

        // GET: Files/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Files files = db.Files.Find(id);
            if (files == null)
            {
                return HttpNotFound();
            }
            return View(files);
        }

        // POST: Files/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            Files files = db.Files.Find(id);
            db.Files.Remove(files);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
