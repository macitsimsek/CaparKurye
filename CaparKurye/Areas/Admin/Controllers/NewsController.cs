﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CaparKurye.AC;
using System.IO;

namespace CaparKurye.Areas.Admin.Controllers
{
    public class NewsController : Controller
    {
        private AC.AppContext db = new AC.AppContext();

        public bool UserCheck()
        {
            if (Session["adminID"] != null && Session["adminName"] != null)
            {
                TempData["Message"] = null;
                return true;
            }
            else
            {
                TempData["Message"] = "Lütfen Admin Girişi Yapınız";
                return false;
            }
        }

        // GET: News
        public ActionResult Index()
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            var news = db.News.Include(n => n.Pictures);
            return View(news.ToList());
        }

        // GET: News/Details/5
        public ActionResult Details(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        // GET: News/Create
        public ActionResult Create()
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            ViewBag.pictureID = new SelectList(db.Pictures, "pictureID", "name");
            return View();
        }

        // POST: News/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "title,subtitle,summary,content,pictureID")] News news, HttpPostedFileBase file)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    if (file.ContentLength > 0)
                    {
                        string _FileName = Path.GetFileNameWithoutExtension(file.FileName) + "-" + DateTime.Now.AddHours(11).ToString("MM-dd-yyyy-hh-mm-ss") + Path.GetExtension(file.FileName);
                        string _path = Path.Combine(Server.MapPath("~/public/UploadedFiles"), _FileName);
                        file.SaveAs(_path);
                        Pictures pictures = new Pictures();
                        pictures.title = file.FileName;
                        pictures.name = file.FileName;
                        db.Pictures.Add(pictures);
                        db.SaveChanges();
                        news.pictureID = pictures.pictureID;
                    }
                }
                news.date = DateTime.Now;
                db.News.Add(news);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.pictureID = new SelectList(db.Pictures, "pictureID", "name", news.pictureID);
            return View(news);
        }

        // GET: News/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            ViewBag.pictureID = new SelectList(db.Pictures, "pictureID", "name", news.pictureID);
            return View(news);
        }

        // POST: News/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "newsID,title,subtitle,summary,content,pictureID")] News news, HttpPostedFileBase file)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    if (file.ContentLength > 0)
                    {
                        string _FileName = Path.GetFileNameWithoutExtension(file.FileName) + "-" + DateTime.Now.AddHours(11).ToString("MM-dd-yyyy-hh-mm-ss") + Path.GetExtension(file.FileName);
                        string _path = Path.Combine(Server.MapPath("~/public/UploadedFiles"), _FileName);
                        file.SaveAs(_path);
                        Pictures pictures = new Pictures();
                        pictures.title = file.FileName;
                        pictures.name = file.FileName;
                        db.Pictures.Add(pictures);
                        db.SaveChanges();
                        news.pictureID = pictures.pictureID;
                    }
                }
                news.title = news.title.ToString().Trim();
                news.summary = news.summary.ToString().Trim();
                news.subtitle = news.subtitle.ToString().Trim();
                news.content = news.content.ToString().Trim();
                news.date = DateTime.Now;
                db.Entry(news).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.pictureID = new SelectList(db.Pictures, "pictureID", "name", news.pictureID);
            return View(news);
        }

        // GET: News/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        // POST: News/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            News news = db.News.Find(id);
            db.News.Remove(news);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
