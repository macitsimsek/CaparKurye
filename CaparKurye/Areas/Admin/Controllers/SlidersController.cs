﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CaparKurye.AC;
using System.IO;
using System.Globalization;

namespace CaparKurye.Areas.Admin.Controllers
{
    public class SlidersController : Controller
    {
        private AC.AppContext db = new AC.AppContext();

        public bool UserCheck()
        {
            if (Session["adminID"] != null && Session["adminName"] != null)
            {
                TempData["Message"] = null;
                return true;
            }
            else
            {
                TempData["Message"] = "Lütfen Admin Girişi Yapınız";
                return false;
            }
        }

        // GET: Sliders
        public ActionResult Index()
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            var sliders = db.Sliders.Include(s => s.Pictures);
            return View(sliders.ToList());
        }

        // GET: Sliders/Details/5
        public ActionResult Details(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sliders sliders = db.Sliders.Find(id);
            if (sliders == null)
            {
                return HttpNotFound();
            }
            return View(sliders);
        }

        // GET: Sliders/Create
        public ActionResult Create()
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            ViewBag.pictureID = new SelectList(db.Pictures, "pictureID", "name");
            return View();
        }

        // POST: Sliders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "sliderID,title,content,pictureID,url,order")] Sliders sliders, HttpPostedFileBase file)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    if (file.ContentLength > 0)
                    {
                        string _FileName = Path.GetFileNameWithoutExtension(file.FileName) + "-" + DateTime.Now.AddHours(11).ToString("MM-dd-yyyy-hh-mm-ss") + Path.GetExtension(file.FileName);
                        string _path = Path.Combine(Server.MapPath("~/public/UploadedFiles"), _FileName);
                        file.SaveAs(_path);
                        Pictures pictures = new Pictures();
                        pictures.title = file.FileName;
                        pictures.name = file.FileName;
                        db.Pictures.Add(pictures);
                        db.SaveChanges();
                        sliders.pictureID = pictures.pictureID;
                    }
                }
                db.Sliders.Add(sliders);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.pictureID = new SelectList(db.Pictures, "pictureID", "name", sliders.pictureID);
            return View(sliders);
        }

        // GET: Sliders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sliders sliders = db.Sliders.Find(id);
            if (sliders == null)
            {
                return HttpNotFound();
            }
            ViewBag.pictureID = new SelectList(db.Pictures, "pictureID", "name", sliders.pictureID);
            return View(sliders);
        }

        // POST: Sliders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "sliderID,title,content,pictureID,url,order")] Sliders sliders, HttpPostedFileBase file)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    if (file.ContentLength > 0)
                    {
                        string _FileName = Path.GetFileNameWithoutExtension(file.FileName) + "-" + DateTime.Now.AddHours(11).ToString("MM-dd-yyyy-hh-mm-ss") + Path.GetExtension(file.FileName);
                        string _path = Path.Combine(Server.MapPath("~/public/UploadedFiles"), _FileName);
                        file.SaveAs(_path);
                        Pictures pictures = new Pictures();
                        pictures.title = file.FileName;
                        pictures.name = file.FileName;
                        db.Pictures.Add(pictures);
                        db.SaveChanges();
                        sliders.pictureID = pictures.pictureID;
                    }
                }
                db.Entry(sliders).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.pictureID = new SelectList(db.Pictures, "pictureID", "name", sliders.pictureID);
            return View(sliders);
        }

        // GET: Sliders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sliders sliders = db.Sliders.Find(id);
            if (sliders == null)
            {
                return HttpNotFound();
            }
            return View(sliders);
        }

        // POST: Sliders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            Sliders sliders = db.Sliders.Find(id);
            db.Sliders.Remove(sliders);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
