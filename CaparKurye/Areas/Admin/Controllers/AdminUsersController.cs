﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CaparKurye.AC;

namespace CaparKurye.Areas.Admin.Controllers
{
    public class AdminUsersController : Controller
    {
        private AC.AppContext db = new AC.AppContext();

        public bool UserCheck()
        {
            if (Session["adminID"] != null && Session["adminName"] != null)
            {
                TempData["Message"] = null;
                return true;
            }
            else
            {
                TempData["Message"] = "Lütfen Admin Girişi Yapınız";
                return false;
            }
        }


        public bool UserCheckBasic()
        {
            if (Session["adminID"] != null && Session["adminName"] != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        // GET: Admins/SearchUser
        public ActionResult SearchUser(string search)
        {
            if (!this.UserCheck()) return Json(new { code = 400, message = "Üye Girişi Yapınız" }, JsonRequestBehavior.AllowGet);

            var users = db.Users.Where(x=> x.nameSurname.Contains(search) || x.email.Contains(search) || x.phone.Contains(search)).Select(x=> new { x.email, x.type, x.nameSurname,x.phone, x.userID, x.date }).ToList();

            return Json(new { code = 200, data = users }, JsonRequestBehavior.AllowGet);
        }


        // GET: UsersAdmin
        public ActionResult Index()
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            return View(db.Users.OrderByDescending(x=>x.date).ToList());
        }

        // GET: UsersAdmin/Details/5
        public ActionResult Details(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // GET: UsersAdmin/Create
        public ActionResult Create()
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            return View(db.Users.OrderByDescending(x => x.userID).ToList());
        }

        // POST: UsersAdmin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult Create([Bind(Include = "userID,type,taxAdministration,taxNumber,TCNumber,authorizedNameSurname,companyAddress,phone,email,password,nameSurname,date")] Users users)
        {
            if (!this.UserCheck()) return Json(new { code = 400, message = "Admin Girişi Yapınız"});
            var obj = db.Users.Where(a => a.email.Equals(users.email)).FirstOrDefault();
            if (obj == null)
            {
                users.phone = users.phone.Trim();
                users.email = users.email.Trim();
                users.password = users.password.Trim();
                users.nameSurname = users.nameSurname.Trim();
                users.date = DateTime.Now.AddHours(11);
                if (users.type == "individual")
                {
                    users.taxAdministration = "";
                    users.taxNumber = null;
                    users.companyAddress = "";
                    users.authorizedNameSurname = "";
                    users.TCNumber = users.TCNumber.Trim();
                }
                else if (users.type == "corporate")
                {
                    users.taxAdministration = users.taxAdministration.Trim();
                    users.authorizedNameSurname = users.authorizedNameSurname.Trim();
                    users.companyAddress = users.companyAddress.Trim();
                    users.taxNumber = users.taxNumber.Trim();
                    users.TCNumber = null;
                }
                if (ModelState.IsValid)
                {
                    db.Users.Add(users);
                    db.SaveChanges();
                    return Json(new { code = 200,  message = "Üye Kaydı Başarıyla Tamamlandı" });
                }
                else
                {
                    return Json(new { code = 400, message = "Eksik ya da Hatalı Giriş Yaptınız." });
                }
            }
            else
            {
                return Json(new { code = 400, message = "Email Kullanılıyor" });
            }
        }

        // GET: UsersAdmin/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!this.UserCheckBasic()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // POST: UsersAdmin/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "userID,type,taxAdministration,taxNumber,TCNumber,authorizedNameSurname,companyAddress,phone,email,password,nameSurname,date")] Users users)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            int userID = users.userID;
            var obj = db.Users.Where(a => a.email.Equals(users.email) && a.userID != userID).FirstOrDefault();
            if (obj == null)
            {
                Users user = db.Users.Find(userID);
                if (users.type == "individual")
                {
                    user.TCNumber = users.TCNumber.Trim();
                    user.taxNumber = null;
                    user.taxAdministration = null;
                    user.authorizedNameSurname = null;
                    user.companyAddress = null;
                }
                else if (users.type == "corporate")
                {
                    user.TCNumber = null;
                    user.taxNumber = users.taxNumber.Trim();
                    user.taxAdministration = users.taxAdministration.Trim();
                    user.authorizedNameSurname = users.authorizedNameSurname.Trim();
                    user.companyAddress = users.companyAddress.Trim();
                }
                user.userID = userID;
                user.type = users.type;
                user.phone = users.phone.Trim();
                user.email = users.email.Trim();
                user.password = users.password.Trim();
                user.nameSurname = users.nameSurname.Trim();
                if (ModelState.IsValid)
                {
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["code"] = 200;
                    TempData["Message"] = "Düzenleme Başarılı";
                    return RedirectToAction("Edit");
                }
                else
                {
                    TempData["code"] = 400;
                    TempData["Message"] = "Düzenleme Başarısız";
                    return RedirectToAction("Edit");
                }
            }
            else
            {
                TempData["code"] = 400;
                TempData["Message"] = "E-Mail Adresi Kullanılıyor";
                return RedirectToAction("Edit");
            }
        }

        // GET: UsersAdmin/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Users users = db.Users.Find(id);
            if (users == null)
            {
                return HttpNotFound();
            }
            return View(users);
        }

        // POST: UsersAdmin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });
            Users users = db.Users.Find(id);
            db.Users.Remove(users);
            db.SaveChanges();
            return RedirectToAction("Create");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
