﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CaparKurye.AC;

namespace CaparKurye.Areas.Admin.Controllers
{
    public class FaqsController : Controller
    {
        private AC.AppContext db = new AC.AppContext();

        public bool UserCheck()
        {
            if (Session["adminID"] != null && Session["adminName"] != null)
            {
                TempData["Message"] = null;
                return true;
            }
            else
            {
                TempData["Message"] = "Lütfen Admin Girişi Yapınız";
                return false;
            }
        }
        // GET: Faqs
        public ActionResult Index()
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            return View(db.Faqs.ToList());
        }

        // GET: Faqs/Details/5
        public ActionResult Details(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Faqs faqs = db.Faqs.Find(id);
            if (faqs == null)
            {
                return HttpNotFound();
            }
            return View(faqs);
        }

        // GET: Faqs/Create
        public ActionResult Create()
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            return View();
        }

        // POST: Faqs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "faqID,category,question,answer")] Faqs faqs)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            if (ModelState.IsValid)
            {
                db.Faqs.Add(faqs);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(faqs);
        }

        // GET: Faqs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Faqs faqs = db.Faqs.Find(id);
            if (faqs == null)
            {
                return HttpNotFound();
            }
            return View(faqs);
        }

        // POST: Faqs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "faqID,category,question,answer")] Faqs faqs)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            if (ModelState.IsValid)
            {
                db.Entry(faqs).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(faqs);
        }

        // GET: Faqs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Faqs faqs = db.Faqs.Find(id);
            if (faqs == null)
            {
                return HttpNotFound();
            }
            return View(faqs);
        }

        // POST: Faqs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            Faqs faqs = db.Faqs.Find(id);
            db.Faqs.Remove(faqs);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
