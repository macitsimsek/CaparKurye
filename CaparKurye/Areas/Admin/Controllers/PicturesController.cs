﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CaparKurye.AC;
using System.IO;

namespace CaparKurye.Areas.Admin.Controllers
{
    public class PicturesController : Controller
    {
        private AC.AppContext db = new AC.AppContext();

        public bool UserCheck()
        {
            if (Session["adminID"] != null && Session["adminName"] != null)
            {
                TempData["Message"] = null;
                return true;
            }
            else
            {
                TempData["Message"] = "Lütfen Admin Girişi Yapınız";
                return false;
            }
        }

        // GET: Pictures
        public ActionResult Index()
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            return View(db.Pictures.ToList());
        }

        // GET: Pictures/Details/5
        public ActionResult Details(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pictures pictures = db.Pictures.Find(id);
            if (pictures == null)
            {
                return HttpNotFound();
            }
            return View(pictures);
        }

        // GET: Pictures/Create
        public ActionResult Create()
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            return View();
        }

        // POST: Pictures/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "title")] Pictures pictures, HttpPostedFileBase file)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            try
            {
                if (ModelState.IsValid)
                {
                    if (file.ContentLength > 0)
                    {
                        string _FileName = Path.GetFileNameWithoutExtension(file.FileName) + "-" + DateTime.Now.AddHours(11).ToString("MM-dd-yyyy-hh-mm-ss") + Path.GetExtension(file.FileName);
                        string _path = Path.Combine(Server.MapPath("~/public/UploadedFiles"), _FileName);
                        file.SaveAs(_path);
                    }
                    pictures.name = file.FileName;
                    db.Pictures.Add(pictures);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return View();
            }
            return View();
        }


        // GET: Pictures/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pictures pictures = db.Pictures.Find(id);
            if (pictures == null)
            {
                return HttpNotFound();
            }
            return View(pictures);
        }

        // POST: Pictures/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "title")] Pictures pictures, HttpPostedFileBase file)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            try
            {
                if (ModelState.IsValid)
                {
                    int id = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"]);
                    pictures.pictureID = id;
                    if (file != null)
                    {
                        if (file.ContentLength > 0)
                        {
                            string _FileName = Path.GetFileNameWithoutExtension(file.FileName) + "-" + DateTime.Now.AddHours(11).ToString("MM-dd-yyyy-hh-mm-ss") + Path.GetExtension(file.FileName);
                            string _path = Path.Combine(Server.MapPath("~/public/UploadedFiles"), _FileName);
                            file.SaveAs(_path);
                            pictures.name = file.FileName;
                            db.Entry(pictures).State = EntityState.Modified;
                        }
                    }
                    else
                    {
                        Pictures pic = db.Pictures.Find(id);
                        pic.title = pictures.title;
                        db.Entry(pic).State = EntityState.Modified;
                    }

                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch(Exception e)
            {
                return View();
            }
            return View();
        }

        // GET: Pictures/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pictures pictures = db.Pictures.Find(id);
            if (pictures == null)
            {
                return HttpNotFound();
            }
            return View(pictures);
        }

        // POST: Pictures/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!this.UserCheck()) return RedirectToAction("Login", "Admins", new { area = "Admin" });

            Pictures pictures = db.Pictures.Find(id);
            db.Pictures.Remove(pictures);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
