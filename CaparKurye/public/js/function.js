

    $(window).load(function () {


        $('[data-toggle="tooltip"]').tooltip();

			var total = $('.announcementgroup .display').length;
            var sayac = 1;

            function announcement() {
            	setTimeout(function () {
            		$('.announcementgroup .display').fadeOut();
            		sayac++;
                    $('.announcementgroup .display:nth-child(' + sayac + ')').fadeIn();
					if (sayac > total) {
                        sayac = 1;
                        $('.announcementgroup .display:nth-child(1)').fadeIn();
                        announcement();
                    } else {
                        announcement();
                    }
            	}, 5000);
            }

    	announcement();

	var swiper = new Swiper('.slider .swiper-container', {
        pagination: '.slider .swiper-pagination',
        centeredSlides: true,
        paginationClickable: true,
        spaceBetween: 30,
        autoplay: 4000,
        autoplayDisableOnInteraction: true,
    });


    var swiper = new Swiper('.news .swiper-container', {
        nextButton: '.news .swiper-button-next',
        prevButton: '.news .swiper-button-prev',
        paginationClickable: true,
        slidesPerView: 4,
        spaceBetween: 50,
        autoplay: 6000,
        autoplayDisableOnInteraction: true,
        breakpoints: {
            1000: {
                slidesPerView: 3,
                spaceBetween: 40
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    });


    });
	