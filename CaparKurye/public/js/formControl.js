﻿$(function () {
    InputControl();
});

function InputControl() {
    // Number Control
    $(".numberControl").keyup(function () {
        if (this.value.match(/[^0-9 ]/g)) {
            this.value = this.value.replace(/[^0-9 ]/g, '');
        }
    });
    $(".numberControl").on("blur", function () {
        if (this.value.match(/[^0-9 ]/g)) {
            this.value = this.value.replace(/[^0-9 ]/g, '');
        }
    });
    // Number Control


    // Latter Control
    $(".letterControl").keyup(function () {
        if (this.value.match(/[^a-zA-Z ğüşıöçĞÜŞİÖÇ.,]/g)) {
            this.value = this.value.replace(/[^a-zA-Z ğüşıöçĞÜŞİÖÇ.,]/g, '');
        }
    });
    $(".letterControl").on("blur", function () {
        if (this.value.match(/[^a-zA-Z ğüşıöçĞÜŞİÖÇ.,]/g)) {
            this.value = this.value.replace(/[^a-zA-Z ğüşıöçĞÜŞİÖÇ.,]/g, '');
        }
    });
    // Latter Control
}


function formControl(e) {


    var returnStatus = false;

    // Required Control
    $(e).find(".required:visible").addClass("error");
    $(e).find(".requiredSelectpicker:visible").addClass("error");
    $(e).find(".requiredText:visible").addClass("errorText");

    $(e).find(".requiredSelectpicker").each(function () {
        if ($(this).find("select").val() != "" && $(this).find("select").val() != null) {
            $(this).removeClass('error');
        }
    });

    $(e).find(".required:visible").each(function () {
        if ($(this).find(".form-control").val() != "") {
            $(this).removeClass('error');
        }
    });

    // Required Control

    // Checked Control
    $(e).find(".requiredText:visible").each(function () {
        if ($(this).find("input").prop("checked")) {
            $(this).removeClass("errorText");
        }
    });
    // Checked Control

    // Email Control
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
    $(e).find(".emailControl:visible").each(function () {
        $(this).removeClass("error");
        if (!isEmail($(this).find(".form-control").val())) {
            $(this).addClass("emailError");
        }
        else {
            $(this).removeClass("emailError");
        }

    });
    // Email Control

    // Phone Control
    $(e).find(".phoneControl:visible").each(function () {
        $(this).removeClass("error");
        if ($(this).find(".form-control").val().length < 11) {
            $(this).addClass("error");
        }
        else {
            $(this).removeClass("error");
        }

    });
    // Phone Control

    // TC Control
    $(e).find(".TCControl:visible").each(function () {
        $(this).removeClass("error");
        if ($(this).find(".form-control").val().length < 11) {
            $(this).addClass("error");
        }
        else {
            $(this).removeClass("error");
        }

    });
    // TC Control

    // Password Control
    $(e).find(".passwordControl:visible").each(function () {
        $(this).removeClass("error");

        if ($(this).find(".form-control").val().length < 9 || !$(this).find(".form-control").val().match(/[^0-9]/g) || !$(this).find(".form-control").val().match(/[^a-zA-Z ğüşıöçĞÜŞİÖÇ.,]/g)) {
            $(this).addClass("error");
        }
        else {
            $(this).removeClass("error");
        }

    });
    // Password Control

    if (!$(e).find(".required:visible").hasClass("error") && !$(e).find(".requiredSelectpicker:visible").hasClass("error") && !$(e).find(".passwordControl:visible").hasClass("error") && !$(e).find(".phoneControl:visible").hasClass("error") && !$(e).find(".TCControl:visible").hasClass("error") && !$(e).find(".requiredText:visible").hasClass("errorText") && !$(e).find(".required:visible").hasClass("emailError")) { returnStatus = true; }

    return returnStatus;

}




