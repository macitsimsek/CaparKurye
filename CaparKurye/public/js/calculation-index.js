var Sigorta = 0;
var Nerden;
var Nereye;
var GonderiTuru;
var GonderiTipi = 2;
var GonderiAdet = 1;
var ToplamKm;
var MerkezGonderenKm;
var GonderenAliciKm;



function cityControl(e,type)
{
    if(type == "city")
    {
        $(e).parent().hide();
        $(e).parent().next(".district").addClass("active");
    }
    else if(type == "district"){
        $(e).parent().removeClass("active");
        $(e).parent().find("input").val("");
        $(e).parent().prev(".city").fadeIn();
    }

}

function mapModal(){
    $('#mapModal').modal('show');
    setTimeout(function(){ MapCreate(); }, 300);
}

// Değerleri Kontrol
function Control(){


    if($("#Nerden").val() != ""){ $("#nerdenIcon").addClass("active"); } else{$("#nerdenIcon").removeClass("active");}
    if($("#Nereye").val() != ""){ $("#nereyeIcon").addClass("active"); } else{$("#nereyeIcon").removeClass("active");}

    Nerden = $("#NerdenIlce").val() + ", " + $("#Nerden").val();
    Nereye = $("#NereyeIlce").val() + ", " + $("#Nereye").val();
    GonderiTuru = $("#GonderiTuru").val();

	if($("#Nerden").val() != "" && $("#Nereye").val() != "" && GonderiTuru != "" && GonderiTipi != "" && GonderiAdet != ""){
		
		var returnDurum = true;
		if(GonderiTuru == "Teklif"){
            returnDurum = false;
            $(".paymentContainer").hide();
            $(".calculation>.text").fadeOut();
            $(".total").addClass("active").hide().fadeIn();
			$("#notfound").hide();
			$("#bid").fadeIn();
		}

		if(returnDurum == true){ 
            $(".total").addClass("active").hide().fadeIn();
			$("#bid").hide();
            $(".motorcycle").addClass("active");
            $(".calculation>.text").fadeOut();
			MapCreate();
		}
            
	}

}

// Harita Oluştur
function MapCreate(){

	// Harita Bilgileri
	var map = new google.maps.Map(document.getElementById('map'), {zoom: 12, center: {lat:38.443858, lng: 27.130023}});
	var directionsService = new google.maps.DirectionsService;
	var directionsDisplay = new google.maps.DirectionsRenderer({ draggable: false, map: map, polylineOptions: {strokeWeight: 3, strokeOpacity: 0.6, strokeColor:  '#1688e3'} });


	// KM Hesapla
	directionsDisplay.addListener('directions_changed', function() { KmCalculation(directionsDisplay.getDirections()); });

	// Haritada Çiz
	MapDraw(Nerden, Nereye, directionsService, directionsDisplay);

	// Adres göster
	$("#nerdenAdres").html(Nerden);
	$("#nereyeAdres").html(Nereye);

}


// Haritada Çiz
function MapDraw(nerden, nereye, service, display){
        if(GonderiTuru == "Imzalı"){
            $(".imzaliGonderim").show();
            service.route({
              waypoints: [
                {
                  location: nerden,
                  stopover: true
                },
                {
                  location: nereye,
                  stopover: true
                },
              ],
              origin: '38.462662, 27.167610',
              destination: nerden,
              travelMode: 'DRIVING',
              avoidTolls: true
            }, function(response, status) {
              if (status === 'OK') {
                display.setDirections(response);
                $(".paymentContainer").fadeIn();
                $("#notfound").hide();
              }
              else{
                    $(".paymentContainer").hide();
                    $("#notfound").fadeIn();
              }
            });
        }
        else {
            $(".imzaliGonderim").hide();
            service.route({
              waypoints: [
                {
                  location: nerden,
                  stopover: true
                }
              ],
              origin: '38.462662, 27.167610',
              destination: nereye,
              travelMode: 'DRIVING',
              avoidTolls: true
            }, function(response, status) {
              if (status === 'OK') {
                display.setDirections(response);
                $(".paymentContainer").fadeIn();
                $("#notfound").hide();
              }
              else{
                    $(".paymentContainer").hide();
                    $("#notfound").fadeIn();
              }
            });

        }
}


// KM Hesapla
function KmCalculation(result) {

	// Toplam Km
	ToplamKm = 0;
	var myroute = result.routes[0];
	for (var i = 0; i < myroute.legs.length; i++) {
		ToplamKm += myroute.legs[i].distance.value;
	}

	// Toplam km sadeleştir
	ToplamKm = ToplamKm / 1000;

	// Merkez - Gönderen arası km
	MerkezGonderenKm = (result.routes[0].legs[0].distance.value) / 1000;

	// Gönderen - Alıcı arası km
	GonderenAliciKm = (ToplamKm - MerkezGonderenKm);


	document.getElementById('total').innerHTML = ToplamKm;

	PriceCalculation();

}


// Fiyat Hesapla
function PriceCalculation(){

	// Merkez - Gönderen arası tutar
	var MerkezGonderenTutar = 0; // 10 km altı ücretsiz
	if(MerkezGonderenKm > 30.000 || MerkezGonderenKm > GonderenAliciKm){
		MerkezGonderenTutar = MerkezGonderenKm * 0.16;
	}

	// Gönderen - Alıcı arası tutar (Acilse tutar değişiyor)
	var GönderenAliciTutar = GonderenAliciKm * 0.99;
	//if(GonderiTuru == "Acil"){
	//	GönderenAliciTutar = GonderenAliciKm * 1.74;
	//}


    var kdv_haric = (GonderiTipi * GonderiAdet) + Sigorta + MerkezGonderenTutar + GönderenAliciTutar;

    if (kdv_haric < 10) {
        kdv_haric = 10;
    }

	var kdv = kdv_haric * 18/100;
	var toplam = (kdv_haric + kdv).toFixed(2).replace('.',',');


	console.log("-----------------------");
	console.log("Merkezden Gönderen Arası:"+ MerkezGonderenKm.toFixed(2) + " km - " + (MerkezGonderenTutar).toFixed(2).replace('.',',') + " TL");
	console.log("Göndericiden Alıcı Arası:"+ GonderenAliciKm.toFixed(2) + " km - " + (GönderenAliciTutar).toFixed(2).replace('.',',') + " TL");
	console.log("Toplam KM:"+ ToplamKm + " km");
	console.log("Sigorta Ücreti:"+ Sigorta + " TL");
	console.log("Gonderi Tip Ücreti:"+ GonderiTipi + " TL");
	console.log("Gonderi Adeti:"+ GonderiTipi + " adet");
	console.log("KDV Hariç :"+ kdv_haric.toFixed(2).replace('.',',')+ " TL");
	console.log("KDV Tutar :"+ kdv.toFixed(2).replace('.',',')+ " TL");
	console.log("Toplam Ücret:"+ toplam + " TL");


	document.getElementById('price').innerHTML = toplam;
	$("#totalnonekdv").text(kdv_haric.toFixed(2).replace('.',','));
	$("#totalkdv").text(kdv.toFixed(2).replace('.',','));
	$("#totalPrice").text(toplam);

}