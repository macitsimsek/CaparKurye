var Nerden = "";
var Nereye = "";
var GonderiTuru = "";

// Değerleri Kontrol
function Control(){


    Nerden = $("#NerdenIlce").selectpicker("val");
    Nereye = $("#NereyeIlce").selectpicker("val");
    GonderiTuru = $("#GonderiTuru").selectpicker("val");

    if(Nerden != ""){ $("#nerdenIcon").addClass("active"); } else{$("#nerdenIcon").removeClass("active");}
    
    if(Nereye != ""){ $("#nereyeIcon").addClass("active"); } else{$("#nereyeIcon").removeClass("active");}

	if(Nerden != "" && Nereye != "" && GonderiTuru != ""){
        
        Nerden = $("#NerdenIlce").val().split(";");
        Nerden = Nerden[$("#NereyeIlce option:selected").index()];

        Nereye = $("#NereyeIlce").val().split(";");
        Nereye = Nereye[$("#NerdenIlce option:selected").index()];
        
        
        toplam = Nerden;
        
        if(GonderiTuru == "Imzalı"){
            toplam = toplam*2; 
        }
        
		var returnDurum = true;
        
        $(".paymentContainer").hide();
        $("#bid").hide();
        $("#notfound").hide();
        $(".total").addClass("active").hide().fadeIn();
        $(".motorcycle").removeClass("active");
        $(".calculation>.text").fadeOut();
        $("#totalPrice").text(toplam);
        
		if(GonderiTuru == "Teklif"){
			returnDurum = false;
			$("#bid").fadeIn();
		}

		if(returnDurum == true){
            $(".paymentContainer").fadeIn();
            $(".motorcycle").addClass("active");
		}
        
            
	}
    else{
            $(".paymentContainer").hide();
    }
    

}
