﻿var Sigorta = 0;
var Nerden;
var Nereye;
var AraNoktalar = [];
var GonderiTuru;
var GonderiTipi;
var GonderiAdet;
var ToplamKm;
var MerkezGonderenKm;
var GonderenAliciKm;


$(window).load(function () {
	$('.recipientTemplate').sortable({
		update: function(event, ui) {Control();}
	});

	$('#range').rangeslider({
	    polyfill: false,
	    onInit: function() {
	      var $handle = $('.rangeslider__handle', this.$range);
	      updateHandle($handle[0], this.value);
	    }
	  })
	  .on('input', function(e) {
	    var $handle = $('.rangeslider__handle', e.target.nextSibling);
	    updateHandle($handle[0], this.value);
	});

	function updateHandle(el, val) {
	  el.textContent = val+" TL";
	}


	recipientAdd();

});


function recipientRemove(e){
	var number = $(".recipientTemplate>li:visible").length;
	if(number > 1)
	{
		$(e).parent().fadeOut(300, function() { $(this).remove(); Control(); });
	}

	if(number <= 23){ $("#recipientAddBtn").show(); }

	if(number < 3)
	{
		$("#GonderiTuru option[value=Imzalı]").removeAttr("disabled","disabled");
		$("#GonderiTuru").val("").selectpicker("refresh");
	}
}

function recipientAdd(){
	var number = $(".recipientTemplate>li:visible").length+1;

	if(number <= 23){
		var clone = $("#recipientTemplate").clone();
		clone.attr("id","recipient"+number);
		clone.find(".Ilce").attr("id","Ilce"+number);
		$(".recipientTemplate").append(clone.fadeIn(300));
		$("#recipient"+number).find(".Ilce").selectpicker();
		Control();
		InputControl();

		if(number == 23){
			$("#recipientAddBtn").hide();
		}	
	}
	if(number > 1)
	{
		$("#GonderiTuru option[value=Imzalı]").attr("disabled","disabled");
		$("#GonderiTuru").val("").selectpicker("refresh");
	}
}


function cityControl(e,type)
{
    if(type == "city")
    {
        $(e).parent().hide();
        $(e).parent().next(".district").addClass("active");
    }
    else if(type == "district"){
        $(e).parent().removeClass("active");
        $(e).parent().find("input").val("");
        $(e).parent().prev(".city").fadeIn();
    }

}

function mapModal(){
    $('#mapModal').modal('show');
    setTimeout(function(){ MapCreate(); }, 300);
}

// Değerleri Kontrol
function Control(){
	$(".paymentContainer").hide();

    Nerden = $("#NerdenIlce").val() + ", " + $("#Nerden").val();
    GonderiTuru = $("#GonderiTuru").val();
    GonderiTipi = $("#GonderiTipi option:selected").attr("data-fiyat");
	GonderiAdet = $("#GonderiAdet").val();

	$("#AdressContainer").html("");
	var number = $(".recipientTemplate>li:visible").length;
	// Birden fazla alıcı varsa
	if(GonderiTuru != "Imzalı"){

		var alphabet = 66;
		AraNoktalar = [{location: Nerden ,stopover: true}];

		$("#AdressContainer").append('<div class="col-xs-12"><div class="map-pin">B</div><div class="map-address"><small>Gönderen</small><span>'+ Nerden +'</span></div></div>');
			
		$(".recipientTemplate>li:visible").each(function(){
			var Ilce = $(this).find(".Ilce").selectpicker("val");
			var Semt = $(this).find(".Semt").val();
			var Adres = Ilce+", "+Semt;

			// Semt boş bırakıldıysa alıcı olarak eklemeyecek
			if(Semt != ""){
				alphabet++;
				AraNoktalar.push({location: Adres ,stopover: true});
				$("#AdressContainer").append('<div class="col-xs-12"><div class="map-bar"><div class="icon"><i class="fa fa-long-arrow-down" aria-hidden="true"></i></div></div></div><div class="col-xs-12"><div class="map-pin">'+ String.fromCharCode(alphabet) +'</div><div class="map-address"><small>Alıcı</small><span>'+ Adres +'</span></div></div>');
			}
		});

		Nereye = AraNoktalar[AraNoktalar.length-1]['location']; // Sonuncu Adresi ekle
		AraNoktalar.splice(AraNoktalar.length-1); // Sonuncu Adresi çıkar
	}
	else{
		AraNoktalar = [];
		var Ilce = $(".recipientTemplate>li:visible").find(".Ilce").selectpicker("val");
		var Semt = $(".recipientTemplate>li:visible").find(".Semt").val();
		Nereye = Ilce+", "+Semt;

		$("#AdressContainer").append('<div class="col-xs-12"><div class="map-pin">B/D</div><div class="map-address"><small>Gönderen</small><span>'+ Nerden +'</span></div></div>');
		$("#AdressContainer").append('<div class="col-xs-12"><div class="map-bar"><div class="icon"><i class="fa fa-long-arrow-down" aria-hidden="true"></i><i class="fa fa-long-arrow-up" aria-hidden="true"></i></div></div></div><div class="col-xs-12"><div class="map-pin">C</div><div class="map-address"><small>Alıcı</small><span>'+ Nereye +'</span></div></div>');
	}


    if ($("#Nerden").val() != "" && Nereye != ", " && GonderiTuru !== "" && GonderiTipi != undefined && GonderiAdet != ""){
		
		var returnDurum = true;

		if(GonderiTipi == "Teklif" || GonderiTuru == "Teklif"){
			returnDurum = false;
			$(".paymentContainer").hide();
			$("#notfound").hide();
			$("#bid").fadeIn();
		}

		// Eğer 
		if(returnDurum == true){
			$("#bid").hide();
			MapCreate();
		}
            
	}

}

// Harita Oluştur
function MapCreate(){

	// Harita Bilgileri
	var map = new google.maps.Map(document.getElementById('map'), {zoom: 12, center: {lat:38.443858, lng: 27.130023}});
	var directionsService = new google.maps.DirectionsService;
	var directionsDisplay = new google.maps.DirectionsRenderer({ draggable: false, map: map, polylineOptions: {strokeWeight: 3, strokeOpacity: 0.6, strokeColor:  '#1688e3'} });


	// KM Hesapla
	directionsDisplay.addListener('directions_changed', function() { KmCalculation(directionsDisplay.getDirections()); });

	// Haritada Çiz
	MapDraw(Nerden, Nereye, directionsService, directionsDisplay);

}


// Haritada Çiz
function MapDraw(nerden, nereye, service, display){
        if(GonderiTuru == "Imzalı"){
            service.route({
              waypoints: [
                {
                  location: nerden,
                  stopover: true
                },
                {
                  location: nereye,
                  stopover: true
                },
              ],
              origin: '38.462662, 27.167610',
              destination: nerden,
              travelMode: 'DRIVING',
              avoidTolls: true
            }, function(response, status) {
              if (status === 'OK') {
                display.setDirections(response);
                $(".paymentContainer").fadeIn();
                $("#notfound").hide();
              }
              else{
                    $(".paymentContainer").hide();
                    $("#notfound").fadeIn();
              }
            });
        }
        else{
            service.route({
              origin: '38.462662, 27.167610',
              waypoints: AraNoktalar,
              destination: nereye,
              travelMode: 'DRIVING',
              avoidTolls: true,
            }, function(response, status) {
              if (status === 'OK') {
                display.setDirections(response);
                $(".paymentContainer").fadeIn();
                $("#notfound").hide();
              }
              else{
                    $(".paymentContainer").hide();
                    $("#notfound").fadeIn();
              }
            });
        }
}


// KM Hesapla
function KmCalculation(result) {

	// Toplam Km
	ToplamKm = 0;
	var myroute = result.routes[0];
	for (var i = 0; i < myroute.legs.length; i++) {
		ToplamKm += myroute.legs[i].distance.value;
	}

	// Toplam km sadeleştir
	ToplamKm = ToplamKm / 1000;

	// Merkez - Gönderen arası km
	MerkezGonderenKm = (result.routes[0].legs[0].distance.value) / 1000;

	// Gönderen - Alıcı arası km
	GonderenAliciKm = (ToplamKm - MerkezGonderenKm);


	document.getElementById('total').innerHTML = ToplamKm;

	PriceCalculation();

}


// Fiyat Hesapla
function PriceCalculation(){

	// Merkez - Gönderen arası tutar
	var MerkezGonderenTutar = 0; // 30 km altı ücretsiz fakat Merkezden Gönderen arası küçükse Gönderen alıcı arasından
	
    if(MerkezGonderenKm > 30.000 && MerkezGonderenKm > GonderenAliciKm){
		MerkezGonderenTutar = MerkezGonderenKm * 0.16;
	}

	// Gönderen - Alıcı arası tutar (Acilse tutar değişiyor)
	var GönderenAliciTutar = GonderenAliciKm * 0.99;
    
	if(GonderiTuru == "Acil"){
		GönderenAliciTutar = GonderenAliciKm * 0.99;
	}

	var kdv_haric = (GonderiTipi * GonderiAdet) + Sigorta + MerkezGonderenTutar + GönderenAliciTutar;

	// Çoklu gönderim varsa %5 indirim uygula
	$("#Indırım").hide();
	if($(".recipientTemplate>li:visible").length > 1)
	{
		kdv_haric = kdv_haric - (kdv_haric * 5/100);
		$("#Indırım").show();
    }

    if (kdv_haric < 10) {
        kdv_haric = 10;
    }

	var kdv = kdv_haric * 18/100;

	var toplam = (kdv_haric + kdv).toFixed(2).replace('.',',');


	//console.log("-----------------------");
	//console.log("Merkezden Gönderen Arası:"+ MerkezGonderenKm.toFixed(2) + " km - " + (MerkezGonderenTutar).toFixed(2).replace('.',',') + " TL");
	//console.log("Göndericiden Alıcı Arası:"+ GonderenAliciKm.toFixed(2) + " km - " + (GönderenAliciTutar).toFixed(2).replace('.',',') + " TL");
	//console.log("Toplam KM:"+ ToplamKm + " km");
	//console.log("Sigorta Ücreti:"+ Sigorta + " TL");
	//console.log("Gonderi Tip Ücreti:"+ GonderiTipi + " TL");
	//console.log("Gonderi Adeti:"+ GonderiAdet + " adet");
	//console.log("KDV Hariç :"+ kdv_haric.toFixed(2).replace('.',',')+ " TL");
	//console.log("KDV Tutar :"+ kdv.toFixed(2).replace('.',',')+ " TL");
	//console.log("Toplam Ücret:"+ toplam + " TL");


	document.getElementById('price').innerHTML = toplam;
	$("#totalnonekdv").text(kdv_haric.toFixed(2).replace('.',','));
	$("#totalkdv").text(kdv.toFixed(2).replace('.',','));
	$("#totalPrice").text(toplam);

}