﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaparKurye.Models
{
    public class ContactMail
    {
        public string nameSurname { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string message { get; set; }
    }
}