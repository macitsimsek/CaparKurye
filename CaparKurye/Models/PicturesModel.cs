﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CaparKurye.Models
{
    public class PicturesModel
    {
        [Key]
        public int pictureID { get; set; }

        [Required]
        [Display(Name = "Ad")]
        public string name { get; set; }

        [Required]
        [Display(Name = "Başlık")]
        public string title { get; set; }
    }
}