﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CaparKurye.Models
{
    public class HumanResourcesMail
    {
        public string position { get; set; }
        public string TCNumber { get; set; }
        public string nameSurname { get; set; }
        public string gender { get; set; }
        public string status { get; set; }
        public string birthdate { get; set; }
        public string birthplace { get; set; }
        public string education { get; set; }
        public string military { get; set; }
        public string driverLicance { get; set; }
        public string smoking { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
    }
}