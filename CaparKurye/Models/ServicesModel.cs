﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CaparKurye.Models
{
    public class ServicesModel
    {
        [Key]
        public int serviceID { get; set; }

        [Required]
        [Display(Name = "Sıra")]
        public int order { get; set; }

        [Required]
        [Display(Name = "Servis İsmi")]
        [MaxLength(250)]
        public string serviceName { get; set; }

        [Required]
        [Display(Name = "Açıklama")]
        public string description { get; set; }

        [Required]
        [Display(Name = "Tip")]
        [MaxLength(50)]
        public string type { get; set; }

        [Display(Name = "Fiyat")]
        [Required(ErrorMessage = "Fiyat alanı gerekli")]
        public string cost { get; set; }
    }
}