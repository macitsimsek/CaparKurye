﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CaparKurye.Models
{
    public class UsersModel
    {
        [Key]
        public int userID { get; set; }

        [Required]
        [MaxLength(50)]
        [Display(Name = "Üyelik Tipi")]
        public string type { get; set; }

        [MaxLength(50)]
        [Display(Name = "Vergi Dairesi")]
        public string taxAdministration { get; set; }

        [MaxLength(50)]
        [Display(Name = "Vergi No")]
        public string taxNumber { get; set; }

        [MaxLength(11)]
        [Display(Name = "TC Kimlik Numarası")]
        public string TCNumber { get; set; }

        [MaxLength(100)]
        [Display(Name = "Yetkili Adı ve Soyadı")]
        public string authorizedNameSurname { get; set; }

        [MaxLength(250)]
        [Display(Name = "Şirket Adresi")]
        public string companyAddress { get; set; }

        [Display(Name = "Telefon")]
        [MinLength(11)]
        [MaxLength(11)]
        public string phone { get; set; }

        [Required]
        [MaxLength(250)]
        [Display(Name = "E-mail")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string email { get; set; }

        [Required]
        [MinLength(6)]
        [MaxLength(16)]
        [DataType(DataType.Password)]
        [Display(Name = "Şifre")]
        public string password { get; set; }

        [Required]
        [MaxLength(100)]
        [Display(Name = "Ad ve Soyad")]
        public string nameSurname { get; set; }

        public DateTime date { get; set; }
    }
}