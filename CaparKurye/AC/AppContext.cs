﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CaparKurye.AC
{
    public class AppContext : DbContext
    {
        public AppContext() : base(@"Mohammadi_capar2Entities")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<AppContext>(null);
            modelBuilder.Entity<Admins>().ToTable("Estate");
            base.OnModelCreating(modelBuilder);
        }


        public DbSet<CaparKurye.Users> Users { get; set; }

        public DbSet<CaparKurye.Services> Services { get; set; }

        public DbSet<CaparKurye.Pictures> Pictures { get; set; }

        public DbSet<CaparKurye.Faqs> Faqs { get; set; }

        public DbSet<CaparKurye.Files> Files { get; set; }

        public DbSet<CaparKurye.News> News { get; set; }

        public DbSet<CaparKurye.Notifications> Notifications { get; set; }

        public DbSet<CaparKurye.Page> Pages { get; set; }

        public DbSet<CaparKurye.Admins> Admins { get; set; }

        public DbSet<CaparKurye.Couriers> Couriers { get; set; }

        public DbSet<CaparKurye.CourierRecievers> CourierRecievers { get; set; }

        public DbSet<CaparKurye.CreditCards> CreditCards { get; set; }

        public DbSet<CaparKurye.Payments> Payments { get; set; }

        public DbSet<CaparKurye.BankAccounts> BankAccounts { get; set; }

        public System.Data.Entity.DbSet<CaparKurye.Settings> Settings { get; set; }

        public System.Data.Entity.DbSet<CaparKurye.Sliders> Sliders { get; set; }
    }
}