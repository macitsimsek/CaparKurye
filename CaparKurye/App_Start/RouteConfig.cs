﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CaparKurye
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Home",                                 // Route name
                "hakkimizda.aspx",                           // URL with parameters
                new { controller = "Home", action = "About" }  // Parameter defaults
            );


            routes.MapRoute(
                "NewsDetail",                                 // Route name
                "haber-detay/{id}",                           // URL with parameters
                new { controller = "Home", action = "NewsDetail", id = UrlParameter.Optional }  // Parameter defaults
            );

            routes.MapRoute(
                "Index",                                 // Route name
                "index.aspx",                           // URL with parameters
                new { controller = "Home", action = "Index" }  // Parameter defaults
            );

            routes.MapRoute(
                "Services",                                 // Route name
                "hizmetlerimiz.aspx",                           // URL with parameters
                new { controller = "Home", action = "Services" }  // Parameter defaults
            ); 

            routes.MapRoute(
                "LastPayments",                                 // Route name
                "gecmis-odemelerim.aspx",                           // URL with parameters
                new { controller = "Home", action = "LastPayments" }  // Parameter defaults
            );

            routes.MapRoute(
                "News",                                 // Route name
                "haberler.aspx",                           // URL with parameters
                new { controller = "Home", action = "News" }  // Parameter defaults
            );

            routes.MapRoute(
                "Contact",                                 // Route name
                "iletisim.aspx",                           // URL with parameters
                new { controller = "Home", action = "Contact" }  // Parameter defaults
            );

            routes.MapRoute(
                "Files",                                 // Route name
                "belgelerimiz.aspx",                           // URL with parameters
                new { controller = "Home", action = "Files" }  // Parameter defaults
            );

            routes.MapRoute(
                "HumanResources",                                 // Route name
                "insankaynaklari.aspx",                           // URL with parameters
                new { controller = "Home", action = "HumanResources" }  // Parameter defaults
            );

            routes.MapRoute(
                "Faq",                                 // Route name
                "sss.aspx",                           // URL with parameters
                new { controller = "Home", action = "Faq" }  // Parameter defaults
            );

            routes.MapRoute(
                "ServiceAgreement",                                 // Route name
                "hizmetsozlesmesi.aspx",                           // URL with parameters
                new { controller = "Home", action = "ServiceAgreement" }  // Parameter defaults
            );

            routes.MapRoute(
                "SignUp",                                 // Route name
                "kayitol.aspx",                           // URL with parameters
                new { controller = "Users", action = "Create" }  // Parameter defaults
            );

            routes.MapRoute(
                "Login",                                 // Route name
                "girisyap.aspx",                           // URL with parameters
                new { controller = "Users", action = "Login" }  // Parameter defaults
            );

            routes.MapRoute(
                "Logout",                                 // Route name
                "logout.aspx",                           // URL with parameters
                new { controller = "Users", action = "Logout" }  // Parameter defaults
            );

            routes.MapRoute(
                "Profile",                                 // Route name
                "profil.aspx",                           // URL with parameters
                new { controller = "Users", action = "Profile" }  // Parameter defaults
            );

            routes.MapRoute(
                "RequestCourier",                                 // Route name
                "kurye-cagir.aspx",                           // URL with parameters
                new { controller = "Home", action = "RequestCourier" }  // Parameter defaults
            );

            routes.MapRoute(
                "ForgotPassword",                                 // Route name
                "sifremiunuttum.aspx",                           // URL with parameters
                new { controller = "Users", action = "ForgotPassword" }  // Parameter defaults
            );
            
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "CaparKurye.Controllers" }
            );

        }
    }
}
