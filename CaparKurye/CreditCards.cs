//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CaparKurye
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class CreditCards
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CreditCards()
        {
            this.Payments = new HashSet<Payments>();
        }

        public int creditCardID { get; set; }
        [Display(Name = "Kart Sahibi Ad Soyad")]
        public string cardOwnerNameSurname { get; set; }
        [Display(Name = "Kart No")]
        public string cardNumber { get; set; }
        [Display(Name = "Kart Son Kullanma Tarihi")]
        public string cardExpiryDate { get; set; }
        [Display(Name = "CVC Code")]
        public string cvcCode { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Payments> Payments { get; set; }
    }
}
